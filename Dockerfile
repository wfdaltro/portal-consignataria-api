FROM adoptopenjdk/openjdk11:alpine
 
ARG JAR_FILE=target/portal-consignataria*.jar
COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]

 
package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.OcorrenciaOperadorEntidadeConsignataria;

public interface OcorrenciaOperadorEntidadeConsignatariaRepository
		extends ConsignusRepository<OcorrenciaOperadorEntidadeConsignataria, Integer> {

}
package br.com.consigus.portalservidor.repository;

import java.util.List;

import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.Portabilidade;

public interface PortabilidadeRepository extends ConsignusRepository<Portabilidade, Long> {

}

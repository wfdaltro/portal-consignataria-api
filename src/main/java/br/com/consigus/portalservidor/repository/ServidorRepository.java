package br.com.consigus.portalservidor.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.Servidor;

@Repository
public interface ServidorRepository extends ConsignusRepository<Servidor, Long> {


	Optional<Servidor> findByCpf(String cpf);

	@Query("select s from Servidor s where s.cpf = :chave or s.matricula = :chave")
	Servidor findByCpfMatricula(@Param("chave") String chave);

}

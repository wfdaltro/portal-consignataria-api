package br.com.consigus.portalservidor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import br.com.consigus.portalservidor.entity.ConfiguracaoEmprestimoConsignataria;

public interface ConfiguracaoEmprestimoConsignatariaRepository  extends ConsignusRepository<ConfiguracaoEmprestimoConsignataria, Long>{

	List<ConfiguracaoEmprestimoConsignataria> findByProduto_pk(Long pk);

	@Query("select cec from ConfiguracaoEmprestimoConsignataria cec where cec.parcelas = ?1 and cec.produto.pk = ?2")
	ConfiguracaoEmprestimoConsignataria findByParcelasProduto_pk(Integer parcelas, Long pk);

}

package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.PropostaCartaoConsignado;

public interface PropostaCartaoConsignadoRepository extends ConsignusRepository<PropostaCartaoConsignado, Long> {

}

package br.com.consigus.portalservidor.repository;

import java.util.List;

import br.com.consigus.portalservidor.entity.DocumentoPropostaEmprestimoConsignado;

public interface DocumentoPropostaEmprestimoConsignadoRepository
		extends ConsignusRepository<DocumentoPropostaEmprestimoConsignado, Integer> {

	List<DocumentoPropostaEmprestimoConsignado> findByPropostaEmprestimoConsignado_pk(Long id);

}

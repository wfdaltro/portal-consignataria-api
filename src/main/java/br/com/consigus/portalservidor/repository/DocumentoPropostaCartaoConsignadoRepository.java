package br.com.consigus.portalservidor.repository;

import java.util.Collection;

import br.com.consigus.portalservidor.entity.DocumentoPropostaCartaoConsignado;

public interface DocumentoPropostaCartaoConsignadoRepository
		extends ConsignusRepository<DocumentoPropostaCartaoConsignado, Integer> {

	Collection<DocumentoPropostaCartaoConsignado> findByPropostaCartaoConsignado_pk(Long id);

}

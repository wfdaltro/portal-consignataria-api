package br.com.consigus.portalservidor.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.consigus.portalservidor.entity.ArquivoDocumentoCartaoConsignado;

public interface ArquivoDocumentoCartaoConsignadoRepository
		extends CrudRepository<ArquivoDocumentoCartaoConsignado, Integer> {

	Optional<ArquivoDocumentoCartaoConsignado> findByDocumento_pk(Integer idAnexo);

}

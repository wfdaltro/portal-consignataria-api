package br.com.consigus.portalservidor.repository;

import java.util.Optional;

import br.com.consigus.portalservidor.entity.ConfiguracaoCartaoConsignataria;

public interface ConfiguracaoCartaoConsignatariaRepository extends ConsignusRepository<ConfiguracaoCartaoConsignataria, Long> {

	Optional<ConfiguracaoCartaoConsignataria> findByProduto_pk(Long pk);

}

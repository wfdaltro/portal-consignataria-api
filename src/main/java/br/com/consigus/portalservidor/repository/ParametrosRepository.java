package br.com.consigus.portalservidor.repository;

import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.Parametros;

@Repository
public interface ParametrosRepository extends ConsignusRepository<Parametros, Integer> {
}

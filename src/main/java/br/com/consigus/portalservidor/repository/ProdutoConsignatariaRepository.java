package br.com.consigus.portalservidor.repository;

import java.util.List;

import org.springframework.data.domain.Sort;

import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.ProdutoConsignataria;
import br.com.consigus.portalservidor.enums.ProdutoConsignado;

public interface ProdutoConsignatariaRepository extends ConsignusRepository<ProdutoConsignataria, Long> {

	List<ProdutoConsignataria> findByEntidadeConsignataria_pk(Long pk, Sort sort);

	ProdutoConsignataria findByProduto(ProdutoConsignado cartao);

	ProdutoConsignataria findByProdutoAndEntidadeConsignataria(ProdutoConsignado cartao,
			EntidadeConsignataria entidadeConsignataria);

	ProdutoConsignataria findByProdutoAndEntidadeConsignataria_pk(ProdutoConsignado emprestimo, Long pk);

}

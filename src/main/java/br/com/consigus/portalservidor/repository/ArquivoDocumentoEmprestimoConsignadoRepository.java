package br.com.consigus.portalservidor.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.consigus.portalservidor.entity.ArquivoDocumentoEmprestimoConsignado;

public interface ArquivoDocumentoEmprestimoConsignadoRepository extends CrudRepository<ArquivoDocumentoEmprestimoConsignado, Integer>{

	Optional<ArquivoDocumentoEmprestimoConsignado> findByDocumento_pk(Integer idAnexo);

}

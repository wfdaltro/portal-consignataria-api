package br.com.consigus.portalservidor.repository;

import java.util.Collection;

import br.com.consigus.portalservidor.entity.ParcelasEmprestimoConsignado;

public interface ParcelasEmprestimoConsignadoRepository extends ConsignusRepository<ParcelasEmprestimoConsignado, Long> {

	Collection<ParcelasEmprestimoConsignado> findByEmprestimoConsignado_pk(Long id);

}

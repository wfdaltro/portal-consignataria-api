package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.PropostaRenegociacaoConsignado;

public interface PropostaRenegociacaoConsignadoRepository extends ConsignusRepository<PropostaRenegociacaoConsignado, Long> {

}

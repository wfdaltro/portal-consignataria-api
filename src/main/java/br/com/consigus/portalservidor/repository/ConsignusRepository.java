package br.com.consigus.portalservidor.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ConsignusRepository<T, K extends Serializable>
		extends JpaRepository<T, K>, QuerydslPredicateExecutor<T> {

	

}

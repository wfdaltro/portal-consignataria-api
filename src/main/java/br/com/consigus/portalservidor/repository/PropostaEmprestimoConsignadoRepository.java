package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.PropostaEmprestimoConsignado;

public interface PropostaEmprestimoConsignadoRepository extends ConsignusRepository<PropostaEmprestimoConsignado, Long> {

}

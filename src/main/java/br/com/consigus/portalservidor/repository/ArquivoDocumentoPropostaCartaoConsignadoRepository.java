package br.com.consigus.portalservidor.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.consigus.portalservidor.entity.ArquivoDocumentoCartaoConsignado;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoPropostaCartaoConsignado;

public interface ArquivoDocumentoPropostaCartaoConsignadoRepository
		extends CrudRepository<ArquivoDocumentoPropostaCartaoConsignado, Integer> {

	Optional<ArquivoDocumentoPropostaCartaoConsignado> findByDocumento_pk(Integer idAnexo);

}

package br.com.consigus.portalservidor.repository;

import java.util.List;

import br.com.consigus.portalservidor.entity.EmprestimoConsignado;

public interface EmprestimoConsignadoRepository  extends ConsignusRepository<EmprestimoConsignado, Long> {

	List<EmprestimoConsignado> findByServidor_pk(Long id);
	
	List<EmprestimoConsignado> findByServidor_pkAndConsignataria_pkNot(Long id, Long consignataria);

}

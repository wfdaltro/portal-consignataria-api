package br.com.consigus.portalservidor.repository;

import java.util.Collection;

import br.com.consigus.portalservidor.entity.DocumentoCartaoConsignado;

public interface DocumentoCartaoConsignadoRespository
		extends ConsignusRepository<DocumentoCartaoConsignado, Integer> {

	Collection<DocumentoCartaoConsignado> findByCartaoConsignado_pk(Long id);

}

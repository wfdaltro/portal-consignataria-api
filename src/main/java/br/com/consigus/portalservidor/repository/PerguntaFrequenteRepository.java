package br.com.consigus.portalservidor.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.PerguntaFrequente;

@Repository
public interface PerguntaFrequenteRepository extends ConsignusRepository<PerguntaFrequente, Long> {

	List<PerguntaFrequente> findByCategoria_pk(Long pk);

}

package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.OcorrenciaCartaoConsignado;

public interface OcorrrenciaCartaoConsignadoRepository
		extends ConsignusRepository<OcorrenciaCartaoConsignado, Integer> {

}

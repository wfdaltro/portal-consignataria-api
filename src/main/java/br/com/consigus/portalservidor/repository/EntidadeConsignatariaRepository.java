package br.com.consigus.portalservidor.repository;

import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.EntidadeConsignataria;

@Repository
public interface EntidadeConsignatariaRepository extends ConsignusRepository<EntidadeConsignataria, Long> {

}

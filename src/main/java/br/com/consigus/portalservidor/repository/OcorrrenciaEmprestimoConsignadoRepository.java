package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.OcorrrenciaEmprestimoConsignado;

public interface OcorrrenciaEmprestimoConsignadoRepository
		extends ConsignusRepository<OcorrrenciaEmprestimoConsignado, Integer> {

}

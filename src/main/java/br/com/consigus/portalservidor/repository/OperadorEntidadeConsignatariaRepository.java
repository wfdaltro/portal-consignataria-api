package br.com.consigus.portalservidor.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;

@Repository
public interface OperadorEntidadeConsignatariaRepository
		extends ConsignusRepository<OperadorEntidadeConsignataria, Long> {

	List<OperadorEntidadeConsignataria> findByEntidadeConsignataria_pk(Long id);

	OperadorEntidadeConsignataria findByCpf(String cpf);
	
	Long countByEntidadeConsignataria_pk(Long id);
	
	OperadorEntidadeConsignataria findByLogin(String login);
	
//	@Query("select new br.com.consigus.portalservidor.security.UsuarioAutenticado(o.pk, o.login, o.matricula, o.nome , o.situacao, o.senhaExpirada, o.senha) from OperadorEntidadeConsignataria o where o.login = ?1")
//	OperadorEntidadeConsignataria autenticaUsuario(String username);



}

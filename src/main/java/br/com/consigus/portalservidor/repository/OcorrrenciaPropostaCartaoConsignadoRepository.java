package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.OcorrrenciaPropostaCartaoConsignado;

public interface OcorrrenciaPropostaCartaoConsignadoRepository extends ConsignusRepository<OcorrrenciaPropostaCartaoConsignado, Integer> {

}

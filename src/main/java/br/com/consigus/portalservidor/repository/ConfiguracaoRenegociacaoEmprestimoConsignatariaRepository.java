package br.com.consigus.portalservidor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import br.com.consigus.portalservidor.entity.ConfiguracaoRenegociacaoEmprestimoConsignataria;

public interface ConfiguracaoRenegociacaoEmprestimoConsignatariaRepository  extends ConsignusRepository<ConfiguracaoRenegociacaoEmprestimoConsignataria, Long>{

	List<ConfiguracaoRenegociacaoEmprestimoConsignataria> findByProduto_pk(Long pk);

	@Query("select cec from ConfiguracaoRenegociacaoEmprestimoConsignataria cec where cec.parcelas = ?1 and cec.produto.pk = ?2")
	ConfiguracaoRenegociacaoEmprestimoConsignataria findByParcelasProduto_pk(Integer parcelas, Long pk);

}

package br.com.consigus.portalservidor.repository;

import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.TrilhaAuditoria;

@Repository
public interface TrilhaAuditoriaRepository extends ConsignusRepository<TrilhaAuditoria, Long> {

}

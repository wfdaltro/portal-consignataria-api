package br.com.consigus.portalservidor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.CategoriaFaq;

@Repository
public interface CategoriaFaqRepository extends ConsignusRepository<CategoriaFaq, Long> {

	@Query("select c from CategoriaFaq c where c.ativo = true")
	List<CategoriaFaq> findAtivos();

}

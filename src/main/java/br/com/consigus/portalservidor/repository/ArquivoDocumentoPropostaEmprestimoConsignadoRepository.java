package br.com.consigus.portalservidor.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.consigus.portalservidor.entity.ArquivoDocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoPropostaEmprestimoConsignado;

public interface ArquivoDocumentoPropostaEmprestimoConsignadoRepository extends CrudRepository<ArquivoDocumentoPropostaEmprestimoConsignado, Integer>{

	Optional<ArquivoDocumentoPropostaEmprestimoConsignado> findByDocumento_pk(Integer idAnexo);

}

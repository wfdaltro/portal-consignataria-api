package br.com.consigus.portalservidor.repository;

import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.CategoriaAviso;

@Repository
public interface CategoriaAvisoRepository extends ConsignusRepository<CategoriaAviso, Long> {

}

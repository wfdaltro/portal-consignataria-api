package br.com.consigus.portalservidor.repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.consigus.portalservidor.entity.Sequencial;

public interface SequencialRepository extends CrudRepository<Sequencial, Integer> {

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query("select s from Sequencial s where s.tipoSequencial = :tipoSequencial")
	Sequencial findForWrite(@Param("tipoSequencial") Integer tipoSequencial);

}
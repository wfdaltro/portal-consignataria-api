package br.com.consigus.portalservidor.repository;

import java.util.List;

import br.com.consigus.portalservidor.entity.OcorrrenciaPropostaEmprestimoConsignado;

public interface OcorrrenciaPropostaEmprestimoConsignadoRepository extends  ConsignusRepository<OcorrrenciaPropostaEmprestimoConsignado, Integer> {
	
	List<OcorrrenciaPropostaEmprestimoConsignado> findByPropostaEmprestimoConsignado_pk(Long id);

}

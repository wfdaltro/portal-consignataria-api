package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.CartaoConsignado;

public interface CartaoConsignadoRepository extends ConsignusRepository<CartaoConsignado, Long> {

}

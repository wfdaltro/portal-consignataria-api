package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.OcorrenciaEntidadeConsignataria;

public interface OcorrenciaEntidadeConsignatariaRepository
		extends ConsignusRepository<OcorrenciaEntidadeConsignataria, Integer> {

}
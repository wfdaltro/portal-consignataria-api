package br.com.consigus.portalservidor.repository;

import br.com.consigus.portalservidor.entity.OcorrenciaServidor;

public interface OcorrenciaServidorRepository extends ConsignusRepository<OcorrenciaServidor, Integer> {

}
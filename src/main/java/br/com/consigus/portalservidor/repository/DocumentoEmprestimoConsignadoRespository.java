package br.com.consigus.portalservidor.repository;

import java.util.Collection;

import br.com.consigus.portalservidor.entity.DocumentoEmprestimoConsignado;

public interface DocumentoEmprestimoConsignadoRespository
		extends ConsignusRepository<DocumentoEmprestimoConsignado, Integer> {

	Collection<DocumentoEmprestimoConsignado> findByEmprestimoConsignado_pk(Long id);

}

package br.com.consigus.portalservidor.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.consigus.portalservidor.entity.Aviso;
import br.com.consigus.portalservidor.enums.DestinatarioAviso;

@Repository
public interface AvisoRepository extends ConsignusRepository<Aviso, Long> {
	
	@Query("SELECT a FROM Aviso a WHERE a.destinatarioAviso = ?1 or a.destinatarioAviso = ?2 or a.destinatarioAviso = ?3")
	Collection<Aviso> findByDestinatarioAviso(DestinatarioAviso operadores, DestinatarioAviso master, DestinatarioAviso todos);

}

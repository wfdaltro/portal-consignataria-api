package br.com.consigus.portalservidor.exception;

public class NegocioException extends Exception {

	public NegocioException(String msg) {
		super(msg);
	}

}

package br.com.consigus.portalservidor.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.entity.Aviso;
import br.com.consigus.portalservidor.enums.DestinatarioAviso;
import br.com.consigus.portalservidor.repository.AvisoRepository;

@Service
public class AvisoService implements ConsignusService<Aviso, Long> {

	@Autowired
	private AvisoRepository avisoRepository;

	@Override
	public Aviso findOne(Long pk) {
		return avisoRepository.findById(pk).get();
	}

	@Override
	public List<Aviso> findAll() {
		return avisoRepository.findAll();
	}

	@Override
	public List<Aviso> findAll(Map<String, String> params) {
		throw new RuntimeException("Ação não permitida.");
	}

	@Override
	public Aviso insert(Aviso entidade) {
		entidade.setAtivo(true);
		return avisoRepository.save(entidade);
	}

	@Override
	public Aviso patch(Long pk, Aviso entidade) {
		Aviso entidadeDb = this.findOne(pk);
		BeanUtils.copyProperties(entidade, entidadeDb, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
				"usuarioAlteracao", "dataHoraAlteracao");
		Aviso en = this.avisoRepository.save(entidadeDb);
		return en;
	}

	@Override
	public void delete(Long pk) {
		Aviso aviso = avisoRepository.findById(pk).get();
		aviso.setAtivo(false);
		avisoRepository.save(aviso);

	}
	
	public Collection<Aviso> findByDestinatarioAviso() {
		return avisoRepository.findByDestinatarioAviso(DestinatarioAviso.OPERADORES,DestinatarioAviso.MASTERS, DestinatarioAviso.TODOS);
	}

}

package br.com.consigus.portalservidor.service;

import java.io.IOException;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrrenciaPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.Parametros;
import br.com.consigus.portalservidor.entity.ParcelasEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.PropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.QPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import br.com.consigus.portalservidor.enums.SituacaoProposta;
import br.com.consigus.portalservidor.enums.TipoSequencial;
import br.com.consigus.portalservidor.mail.EmailHelper;
import br.com.consigus.portalservidor.repository.ArquivoDocumentoPropostaEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.DocumentoEmprestimoConsignadoRespository;
import br.com.consigus.portalservidor.repository.DocumentoPropostaEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.EmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OcorrrenciaPropostaEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ParametrosRepository;
import br.com.consigus.portalservidor.repository.ParcelasEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.PropostaEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.ServidorRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;
import br.com.consigus.portalservidor.utils.JasperHelper;

@Service
public class PropostaEmprestimoConsignadoService {

	@Autowired
	private PropostaEmprestimoConsignadoRepository propostaEmprestimoConsignadoRepository;

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Autowired
	private ServidorRepository servidorRepository;

	@Autowired
	private SequencialService sequencialService;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private ParametrosRepository parametrosRepository;

	@Autowired
	private EmprestimoConsignadoRepository emprestimoConsignadoRepository;

	@Autowired
	private ParcelasEmprestimoConsignadoRepository parcelasEmprestimoConsignadoRepository;

	@Autowired
	private DocumentoPropostaEmprestimoConsignadoRepository documentoPropostaEmprestimoConsignadoRepository;

	@Autowired
	private DocumentoEmprestimoConsignadoRespository documentoEmprestimoConsignadoRepository;

	@Autowired
	private OcorrrenciaPropostaEmprestimoConsignadoRepository ocorrrenciaPropostaEmprestimoConsignadoRepository;

	@Autowired
	private ArquivoDocumentoPropostaEmprestimoConsignadoRepository arquivoDocumentoPropostaEmprestimoConsignadoRepository;

	@Autowired
	private JasperHelper helper;

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public PropostaEmprestimoConsignado insert(@Valid PropostaEmprestimoConsignado entidade)
			throws AddressException, MessagingException {
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		Servidor servidor = servidorRepository.findById(entidade.getServidor().getPk()).get();
		if (entidade.getValorPrestacao().compareTo(servidor.getMargemEmprestimoDisponivel()) == 1) {
			throw new RuntimeException("Servidor nâo possui margem consignável necessária.");
		}
		entidade.setOperadorResponsavel(operadorInclusao);
		entidade.setServidor(servidor);
		entidade.setSituacao(SituacaoProposta.EM_ANALISE);
		entidade.setConsignataria(entidadeConsignataria);
		entidade.setTokenLiberacao(gerarTokenProposta());
		entidade.setNumeroProposta(sequencialService.gerarNumeroProtocolo(TipoSequencial.PROPOSTA_EMPRESTIMO));
		Parametros parametros = parametrosRepository.findById(1).get();
		int diasValidadeProposta = 5;
		if (parametros != null && parametros.getPrazoMaximoAprovacaoContrato() != null) {
			diasValidadeProposta = parametros.getPrazoMaximoAprovacaoContrato();
		}
		entidade.setDataValidadeProposta(LocalDate.now().plusDays(diasValidadeProposta));
		PropostaEmprestimoConsignado propostaSalva = propostaEmprestimoConsignadoRepository.saveAndFlush(entidade);

		sendMail(entidade, servidor);

		try {
			servidor.setMargemEmprestimoReservada(
					servidor.getMargemEmprestimoReservada().add(entidade.getValorPrestacao()));
			servidor.setMargemEmprestimoDisponivel(
					servidor.getMargemEmprestimoDisponivel().subtract(servidor.getMargemEmprestimoReservada()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		servidorRepository.save(servidor);
		DadosOperacao dados = new DadosOperacao();
		dados.setObservacao("Proposta criada.");
		ocorrencia(entidade.getPk(), dados);
		return propostaSalva;
	}

	private void sendMail(PropostaEmprestimoConsignado entidade, Servidor servidor) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					emailHelper.enviarEmail(servidor.getEmail(), "Proposta de emissão de empréstimo  consignado.",
							"O código de validação da proposta número:  " + entidade.getNumeroProposta() + " é: "
									+ entidade.getTokenLiberacao());
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).start();
	}

	private String gerarTokenProposta() {
		return UUID.randomUUID().toString().substring(0, 6);
	}

	public EmprestimoConsignado averbar(Long id, String codigoAverbacao) {
		PropostaEmprestimoConsignado proposta = propostaEmprestimoConsignadoRepository.findById(id).get();
		if (!proposta.getTokenLiberacao().equals(codigoAverbacao)) {
			throw new RuntimeException("O código de averbação informado não é válido.");
		}
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EmprestimoConsignado emprestimo = new EmprestimoConsignado();
		emprestimo.setConsignataria(proposta.getConsignataria());
		emprestimo.setAde(proposta.getNumeroProposta());
		emprestimo.setJurosAoMes(proposta.getJurosMes());
		emprestimo.setParcelasEmAberto(proposta.getParcelas());
		emprestimo.setServidor(proposta.getServidor());
		emprestimo.setSituacao(SituacaoConsignacao.ATIVA);
		emprestimo.setTotalSaldoResidual(proposta.getValorLiberado());
		emprestimo.setTotalDeParcelas(proposta.getParcelas());
		emprestimo.setValorFinanciado(proposta.getValorLiberado());
		emprestimo.setValorPrestacao(proposta.getValorPrestacao());
		emprestimo.setValorTotalFinanciamento(proposta.getValorTotal());
		emprestimoConsignadoRepository.save(emprestimo);
		gerarParcelasEmpretimo(emprestimo);
		gerarDocumentosEmprestimo(proposta, emprestimo);
		proposta.setSituacao(SituacaoProposta.DEFERIDA);
		gravarOorrenciaDeferimentoProposta(proposta);

		Servidor servidor = servidorRepository.findById(proposta.getServidor().getPk()).get();
		servidor.setMargemEmprestimoReservada(
				servidor.getMargemEmprestimoReservada().subtract(proposta.getValorPrestacao()));
		servidorRepository.save(servidor);

		propostaEmprestimoConsignadoRepository.save(proposta);
		return emprestimo;
	}

	private void gravarOorrenciaDeferimentoProposta(PropostaEmprestimoConsignado proposta) {
		OcorrrenciaPropostaEmprestimoConsignado ocorrencia = new OcorrrenciaPropostaEmprestimoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia("Proposta deferida");
		ocorrencia.setPropostaEmprestimoConsignado(proposta);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		ocorrencia.setMotivoOcorrencia(SituacaoProposta.DEFERIDA.getDescricao());
		ocorrrenciaPropostaEmprestimoConsignadoRepository.save(ocorrencia);

	}

	private void gerarDocumentosEmprestimo(PropostaEmprestimoConsignado proposta, EmprestimoConsignado emprestimo) {
		List<DocumentoPropostaEmprestimoConsignado> documentosProposta = documentoPropostaEmprestimoConsignadoRepository
				.findByPropostaEmprestimoConsignado_pk(proposta.getPk());
		if (documentosProposta != null && !documentosProposta.isEmpty()) {
			List<DocumentoEmprestimoConsignado> documentosEmprestimos = new ArrayList<>();
			for (DocumentoPropostaEmprestimoConsignado dProposta : documentosProposta) {
				DocumentoEmprestimoConsignado dEmprestimo = new DocumentoEmprestimoConsignado();
				dEmprestimo.setDataCadastro(LocalDate.now());
				dEmprestimo.setEmprestimoConsignado(emprestimo);
				dEmprestimo.setNome(dProposta.getNome());
				dEmprestimo.setUsuarioCadastro(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
				documentosEmprestimos.add(dEmprestimo);
			}
			documentoEmprestimoConsignadoRepository.saveAll(documentosEmprestimos);
		}
	}

	private void gerarParcelasEmpretimo(EmprestimoConsignado emprestimo) {
		List<ParcelasEmprestimoConsignado> parcelas = new ArrayList<>();
		for (int i = 0; i < emprestimo.getTotalDeParcelas(); i++) {
			ParcelasEmprestimoConsignado parcela = new ParcelasEmprestimoConsignado();
			parcela.setNumero(i);
			parcela.setPaga(false);
			parcela.setValorPrincipal(emprestimo.getValorPrestacao());
			parcela.setDataVencimento(LocalDate.now().plusMonths((i + 1)));
			parcela.setEmprestimoConsignado(emprestimo);
			parcelas.add(parcela);
		}
		parcelasEmprestimoConsignadoRepository.saveAll(parcelas);
	}

	public List<PropostaEmprestimoConsignado> findAll(Map<String, String> params) {
		Iterable<PropostaEmprestimoConsignado> resultado = this.propostaEmprestimoConsignadoRepository
				.findAll(this.getPredicate(params), Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QPropostaEmprestimoConsignado proposta = QPropostaEmprestimoConsignado.propostaEmprestimoConsignado;
		BooleanExpression queryExpression = proposta.pk.isNotNull();
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		BooleanExpression consignataria = proposta.consignataria.pk.eq(entidadeConsignataria.getPk());
		queryExpression = queryExpression.and(consignataria);
		if (params.get("numero") != null && !params.get("numero").isBlank()) {
			BooleanExpression ade = proposta.numeroProposta.eq(params.get("numero"));
			queryExpression = queryExpression.and(ade);
		}

		if (params.get("servidor") != null) {
			BooleanExpression servidor = proposta.servidor.pk.eq(Long.valueOf(params.get("servidor")));
			queryExpression = queryExpression.and(servidor);
		}

		if (params.get("situacao") != null) {
			SituacaoProposta s = SituacaoProposta.get(params.get("situacao"));
			BooleanExpression situacao = proposta.situacao.eq(s);
			queryExpression = queryExpression.and(situacao);
		}
		return queryExpression;
	}

	public void ocorrencia(Long id, @Valid DadosOperacao dadosOperacao) {
		PropostaEmprestimoConsignado propostaEmprestimoConsignado = propostaEmprestimoConsignadoRepository.findById(id)
				.get();
		OcorrrenciaPropostaEmprestimoConsignado ocorrencia = new OcorrrenciaPropostaEmprestimoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia(dadosOperacao.getObservacao());
		ocorrencia.setPropostaEmprestimoConsignado(propostaEmprestimoConsignado);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		if (dadosOperacao.getAcao() == null) {
			propostaEmprestimoConsignado.setSituacao(SituacaoProposta.EM_ANALISE);
			ocorrencia.setMotivoOcorrencia(SituacaoProposta.EM_ANALISE.getDescricao());
		} else {
			if (3 == dadosOperacao.getAcao()) { // indeferir
				propostaEmprestimoConsignado.setSituacao(SituacaoProposta.INDEFERIDA);
				ocorrencia.setMotivoOcorrencia(SituacaoProposta.INDEFERIDA.getDescricao());
				estornarSaldoServidor(propostaEmprestimoConsignado);
			} else if (5 == dadosOperacao.getAcao()) { // devolver
				propostaEmprestimoConsignado.setSituacao(SituacaoProposta.DEVOLVIDA_AO_SOLICITANTE);
				ocorrencia.setMotivoOcorrencia(SituacaoProposta.DEVOLVIDA_AO_SOLICITANTE.getDescricao());
			} else if (4 == dadosOperacao.getAcao()) { // cancelar
				propostaEmprestimoConsignado.setSituacao(SituacaoProposta.CANCELADA_PELO_SOLICITANTE);
				ocorrencia.setMotivoOcorrencia(SituacaoProposta.CANCELADA_PELO_SOLICITANTE.getDescricao());
				estornarSaldoServidor(propostaEmprestimoConsignado);
			}
		}
		ocorrrenciaPropostaEmprestimoConsignadoRepository.save(ocorrencia);
		propostaEmprestimoConsignadoRepository.save(propostaEmprestimoConsignado);

	}

	private void estornarSaldoServidor(PropostaEmprestimoConsignado propostaEmprestimoConsignado) {
		MathContext mc = new MathContext(2);
		Servidor servidor = servidorRepository.findById(propostaEmprestimoConsignado.getServidor().getPk()).get();
		servidor.setMargemEmprestimoReservada(
				servidor.getMargemEmprestimoReservada().subtract(propostaEmprestimoConsignado.getValorPrestacao()));
		servidor.setMargemEmprestimoDisponivel(servidor.getMargemEmprestimoDisponivel().add(propostaEmprestimoConsignado.getValorPrestacao()));
		servidorRepository.save(servidor);
	}

	public PropostaEmprestimoConsignado findOne(Long id) {
		return propostaEmprestimoConsignadoRepository.findById(id).get();
	}

	public void inserirAnexos(Long idEmprestimo, MultipartFile file, String nome) throws IOException {
		PropostaEmprestimoConsignado emprestimoConsignado = propostaEmprestimoConsignadoRepository
				.findById(idEmprestimo).get();
		DocumentoPropostaEmprestimoConsignado doc = new DocumentoPropostaEmprestimoConsignado();
		ArquivoDocumentoPropostaEmprestimoConsignado arquivo = new ArquivoDocumentoPropostaEmprestimoConsignado();
		doc.setDataCadastro(LocalDate.now());
		doc.setNome(nome);
		doc.setPropostaEmprestimoConsignado(emprestimoConsignado);
		doc.setContentType(file.getContentType());
		doc.setTamanho(file.getSize());
		documentoPropostaEmprestimoConsignadoRepository.save(doc);
		arquivo.setArquivo(file.getBytes());
		arquivo.setDocumento(doc);
		arquivo.setContentType(file.getContentType());
		arquivoDocumentoPropostaEmprestimoConsignadoRepository.save(arquivo);

	}

	@Transactional
	public ArquivoDocumentoPropostaEmprestimoConsignado obterAnexo(Integer idAnexo) {
		return arquivoDocumentoPropostaEmprestimoConsignadoRepository.findById(idAnexo).get();

	}

	@Transactional
	public Collection<DocumentoPropostaEmprestimoConsignado> anexos(Long id) {
		return documentoPropostaEmprestimoConsignadoRepository.findByPropostaEmprestimoConsignado_pk(id);
	}

	@Transactional
	public void removerArquivo(Integer id) {
		DocumentoPropostaEmprestimoConsignado documentoEmprestimoConsignado = documentoPropostaEmprestimoConsignadoRepository
				.findById(id).get();
		ArquivoDocumentoPropostaEmprestimoConsignado arquivo = arquivoDocumentoPropostaEmprestimoConsignadoRepository
				.findByDocumento_pk(documentoEmprestimoConsignado.getPk()).get();
		arquivoDocumentoPropostaEmprestimoConsignadoRepository.delete(arquivo);
		documentoPropostaEmprestimoConsignadoRepository.delete(documentoEmprestimoConsignado);
	}

	public List<OcorrrenciaPropostaEmprestimoConsignado> findOcorrencias(Long id) {
		return ocorrrenciaPropostaEmprestimoConsignadoRepository.findByPropostaEmprestimoConsignado_pk(id);
	}

	public byte[] gerarRelatorioAutorizacaoDesconto(Long idProposta) {
		PropostaEmprestimoConsignado proposta = propostaEmprestimoConsignadoRepository.findById(idProposta).get();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nomeServidor", proposta.getServidor().getNome());
		parameters.put("cpfServidor", proposta.getServidor().getCpf());
		parameters.put("consignataria", proposta.getConsignataria().getNome());
		return helper.pdf("termo_autorizacao_emprestimo", parameters, null);

	}

}

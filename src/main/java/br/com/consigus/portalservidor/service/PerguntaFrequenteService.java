package br.com.consigus.portalservidor.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.dto.FaqsDto;
import br.com.consigus.portalservidor.dto.PerguntaDto;
import br.com.consigus.portalservidor.entity.CategoriaFaq;
import br.com.consigus.portalservidor.entity.PerguntaFrequente;
import br.com.consigus.portalservidor.repository.CategoriaFaqRepository;
import br.com.consigus.portalservidor.repository.PerguntaFrequenteRepository;

@Service
public class PerguntaFrequenteService {

	@Autowired
	private PerguntaFrequenteRepository repository;

	@Autowired
	private CategoriaFaqRepository categoriaFaqRepository;

	public List<FaqsDto> findAll() {
		List<FaqsDto> faqs = new ArrayList<>();
		List<CategoriaFaq> categorias = categoriaFaqRepository.findAtivos();
		for (CategoriaFaq c : categorias) {
			FaqsDto faq = new FaqsDto();
			faq.setCategoria(c.getNome());
			List<PerguntaFrequente> perguntas = repository.findByCategoria_pk(c.getPk());
			for (PerguntaFrequente p : perguntas) {
				PerguntaDto pergunta = new PerguntaDto();
				pergunta.setTitulo(p.getTitulo());
				pergunta.setResposta(p.getResposta());
				if (faq.getPerguntas() == null) {
					faq.setPerguntas(new ArrayList<>());
				}
				faq.getPerguntas().add(pergunta);
			}
			faqs.add(faq);
		}
		return faqs;

	}

}

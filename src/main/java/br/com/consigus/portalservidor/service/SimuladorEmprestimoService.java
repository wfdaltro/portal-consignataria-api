package br.com.consigus.portalservidor.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.dto.OfertaEmprestimo;
import br.com.consigus.portalservidor.entity.ConfiguracaoEmprestimoConsignataria;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.ProdutoConsignataria;
import br.com.consigus.portalservidor.entity.PropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.enums.ProdutoConsignado;
import br.com.consigus.portalservidor.repository.ConfiguracaoEmprestimoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.EntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ProdutoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ServidorRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class SimuladorEmprestimoService {

	@Autowired
	private ServidorRepository servidorRepository;

	@Autowired
	private ProdutoConsignatariaRepository produtoConsignatariaRepository;

	@Autowired
	private ConfiguracaoEmprestimoConsignatariaRepository configuracaoEmprestimoConsignatariaRepository;
	

	@Autowired
	private OperadorEntidadeConsignatariaRepository repository;

	public OfertaEmprestimo simularPorValorParcelas(Long idServidor, Double valor, Integer parcelas)
			throws ParseException {
		Servidor servidor = servidorRepository.findById(idServidor).get();
		if (servidor.getMargemEmprestimoDisponivel() == BigDecimal.ZERO) {
			return null;
		}
		
		EntidadeConsignataria entidadeConsignataria = repository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado()).getEntidadeConsignataria();
		
		ProdutoConsignataria produtoEmprestimo = produtoConsignatariaRepository
				.findByProdutoAndEntidadeConsignataria_pk(ProdutoConsignado.EMPRESTIMO, entidadeConsignataria.getPk());
		
		if (produtoEmprestimo == null) {
			throw new RuntimeException("Consignatária não possui produto de empréstimo cadastrado");
		}
		
		
		ConfiguracaoEmprestimoConsignataria configuracaoEmp = configuracaoEmprestimoConsignatariaRepository
				.findByParcelasProduto_pk(parcelas, produtoEmprestimo.getPk());
		
		if (configuracaoEmp == null) {
			throw new RuntimeException("Não existem propostas para o prazo  informado.");
		}
		
		OfertaEmprestimo oferta = new OfertaEmprestimo();
		oferta.setDataSimulacao(LocalDate.now());
		oferta.setJurosMensal(configuracaoEmp.getJuros());
		oferta.setValorParcela(valor);
		oferta.setParcelas(configuracaoEmp.getParcelas());
		oferta.setValorEmprestimo(
				calcularValorFinanciado(valor, configuracaoEmp.getJuros(), configuracaoEmp.getParcelas()));
		oferta.setValorTotal(oferta.getValorParcela() * parcelas);
		return oferta;
	}

	public OfertaEmprestimo simularPorValorTotal(Long idServidor, Double valor, Integer parcelas)
			throws ParseException {
		Servidor servidor = servidorRepository.findById(idServidor).get();
		if (servidor.getMargemEmprestimoDisponivel() == BigDecimal.ZERO) {
			return null;
		}
		ProdutoConsignataria produtoEmprestimo = produtoConsignatariaRepository
				.findByProduto(ProdutoConsignado.EMPRESTIMO);
		ConfiguracaoEmprestimoConsignataria configuracaoEmp = configuracaoEmprestimoConsignatariaRepository
				.findByParcelasProduto_pk(parcelas, produtoEmprestimo.getPk());
		if (configuracaoEmp == null) {
			throw new RuntimeException("Não existem propostas para o prazo  informado.");
		}
		OfertaEmprestimo oferta = new OfertaEmprestimo();
		oferta.setDataSimulacao(LocalDate.now());
		oferta.setJurosMensal(configuracaoEmp.getJuros());
		oferta.setValorEmprestimo(valor);
		oferta.setParcelas(configuracaoEmp.getParcelas());
		oferta.setValorParcela(calcularValorParcelas(valor, configuracaoEmp.getJuros(), configuracaoEmp.getParcelas()));
		oferta.setValorTotal(oferta.getValorParcela() * parcelas);
		return oferta;
	}

	private Double calcularValorParcelas(Double valor, double juros, Integer parcelas) throws ParseException {
		double jurosReal = juros / 100;
		double nc = (Math.pow((1 + jurosReal), parcelas) * jurosReal);
		double dc = (Math.pow((1 + jurosReal), parcelas) - 1);
		double c = nc / dc;
		double total = valor * c;
		String totalStr = new DecimalFormat("###0.000000").format(total);
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		Number number = format.parse(totalStr);
		return number.doubleValue();

	}

	private Double calcularValorFinanciado(Double valor, double juros, Integer parcelas) throws ParseException {
		double jurosReal = juros / 100;
		double nc = (Math.pow((1 + jurosReal), parcelas) * jurosReal);
		double dc = (Math.pow((1 + jurosReal), parcelas) - 1);
		double c = nc / dc;
		double total = valor / c;
		String totalStr = new DecimalFormat("###0.000000").format(total);
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		Number number = format.parse(totalStr);
		return number.doubleValue();

	}

}

package br.com.consigus.portalservidor.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrrenciaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.ParcelasEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.Portabilidade;
import br.com.consigus.portalservidor.entity.QPortabilidade;
import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import br.com.consigus.portalservidor.enums.SituacaoPortabilidade;
import br.com.consigus.portalservidor.enums.TipoRegistroPortabilidade;
import br.com.consigus.portalservidor.mail.EmailHelper;
import br.com.consigus.portalservidor.repository.EmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OcorrrenciaEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ParcelasEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.PortabilidadeRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class PortabilidadeService implements ConsignusService<Portabilidade, Long> {

	@Autowired
	private PortabilidadeRepository portabilidadeRepository;

	@Autowired
	OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Autowired
	private EmprestimoConsignadoRepository emprestimoConsignadoRepository;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private ParcelasEmprestimoConsignadoRepository parcelasEmprestimoConsignadoRepository;

	@Autowired
	private OcorrrenciaEmprestimoConsignadoRepository ocorrrenciaEmprestimoConsignadoRepository;

	@Override
	public Portabilidade findOne(Long pk) {
		return portabilidadeRepository.findById(pk).get();
	}

	@Override
	public List<Portabilidade> findAll() {
		return portabilidadeRepository.findAll();
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public Portabilidade insert(Portabilidade entidade) {
		EntidadeConsignataria consignatatria = getEntidadeConsignataria();
		entidade.setConsignataria(consignatatria);
		EmprestimoConsignado emprestimoConsignado = emprestimoConsignadoRepository
				.findById(entidade.getEmprestimo().getPk()).get();
		entidade.setEmprestimo(emprestimoConsignado);
		entidade.setSituacao(SituacaoPortabilidade.EM_ANALISE);
		entidade.setTokenLiberacao(gerarTokenProposta());
		Portabilidade p = portabilidadeRepository.save(entidade);
		try {
			emailHelper.enviarEmail(emprestimoConsignado.getServidor().getEmail(),
					"Portabilidade de Empréstimo Consignado.", "O código de validação da proposta número:  "
							+ emprestimoConsignado.getAde() + " é: " + p.getTokenLiberacao());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return p;
	}

	@Override
	public Portabilidade patch(Long pk, Portabilidade entidade) {
		throw new RuntimeException("Ação não permitida.");
	}

	@Override
	public void delete(Long pk) {
		throw new RuntimeException("Ação não permitida.");

	}

	public List<Portabilidade> findAll(Map<String, String> params) {
		Iterable<Portabilidade> resultado = this.portabilidadeRepository.findAll(this.getPredicate(params),
				Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QPortabilidade proposta = QPortabilidade.portabilidade;
		BooleanExpression queryExpression = proposta.pk.isNotNull();
		EntidadeConsignataria entidadeConsignataria = getEntidadeConsignataria();
	 
		if (params.get("servidor") != null) {
			BooleanExpression servidor = proposta.emprestimo.servidor.pk.eq(Long.valueOf(params.get("servidor")));
			queryExpression = queryExpression.and(servidor);
		}

		if (params.get("tipo") != null) {
		 TipoRegistroPortabilidade tipoRegistroPortabilidade = TipoRegistroPortabilidade.get(params.get("tipo"));
		 if (TipoRegistroPortabilidade.ENVIADAS.equals(tipoRegistroPortabilidade)) {
				 BooleanExpression servidor = proposta.consignataria.pk.eq(entidadeConsignataria.getPk());
					queryExpression = queryExpression.and(servidor);
		 }
		 if (TipoRegistroPortabilidade.RECEBIDAS.equals(tipoRegistroPortabilidade)) {
			 BooleanExpression servidor = proposta.emprestimo.consignataria.pk.eq(entidadeConsignataria.getPk());
				queryExpression = queryExpression.and(servidor);
		 }
		}

		if (params.get("situacao") != null) {
			SituacaoPortabilidade s = SituacaoPortabilidade.get(params.get("situacao"));
			BooleanExpression situacao = proposta.situacao.eq(s);
			queryExpression = queryExpression.and(situacao);
		}
		return queryExpression;
	}

	private EntidadeConsignataria getEntidadeConsignataria() {
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		return entidadeConsignataria;
	}

	private String gerarTokenProposta() {
		return UUID.randomUUID().toString().substring(0, 6);
	}

	public void finalizar(Long id) {
		Portabilidade portabilidade = portabilidadeRepository.findById(id).get();
		portabilidade.setSituacao(SituacaoPortabilidade.CONCLUIDA);
		portabilidadeRepository.save(portabilidade);
		EmprestimoConsignado emprestimo = new EmprestimoConsignado();
		emprestimo.setConsignataria(portabilidade.getEmprestimo().getConsignataria());
		emprestimo.setAde("9" + portabilidade.getEmprestimo().getAde());
		emprestimo.setJurosAoMes(portabilidade.getJurosAoMes());
		emprestimo.setParcelasEmAberto(portabilidade.getTotalDeParcelas());
		emprestimo.setServidor(portabilidade.getEmprestimo().getServidor());
		emprestimo.setSituacao(SituacaoConsignacao.ATIVA);
		emprestimo.setTotalSaldoResidual(portabilidade.getEmprestimo().getValorFinanciado());
		emprestimo.setTotalDeParcelas(portabilidade.getTotalDeParcelas());
		emprestimo.setValorFinanciado(portabilidade.getEmprestimo().getValorFinanciado());
		emprestimo.setValorPrestacao(portabilidade.getValorPrestacao());
		emprestimo.setValorTotalFinanciamento(
				new BigDecimal(portabilidade.getValorPrestacao().doubleValue() * portabilidade.getTotalDeParcelas()));
		emprestimoConsignadoRepository.save(emprestimo);
		EmprestimoConsignado ep = portabilidade.getEmprestimo();
		ep.setSituacao(SituacaoConsignacao.COMPRADA);
		emprestimoConsignadoRepository.save(ep);
		gerarParcelasEmpretimo(emprestimo);
		ocorrencia(emprestimo);

	}

	private void gerarParcelasEmpretimo(EmprestimoConsignado emprestimo) {
		List<ParcelasEmprestimoConsignado> parcelas = new ArrayList<>();
		for (int i = 0; i < emprestimo.getTotalDeParcelas(); i++) {
			ParcelasEmprestimoConsignado parcela = new ParcelasEmprestimoConsignado();
			parcela.setNumero(i);
			parcela.setPaga(false);
			parcela.setValorPrincipal(emprestimo.getValorPrestacao());
			parcela.setDataVencimento(LocalDate.now().plusMonths((i + 1)));
			parcela.setEmprestimoConsignado(emprestimo);
			parcelas.add(parcela);
		}
		parcelasEmprestimoConsignadoRepository.saveAll(parcelas);
	}

	public void ocorrencia(EmprestimoConsignado emprestimoConsignado) {
		OcorrrenciaEmprestimoConsignado ocorrencia = new OcorrrenciaEmprestimoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia("");
		ocorrencia.setEmprestimoConsignado(emprestimoConsignado);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.COMPRADA.getDescricao());
		ocorrrenciaEmprestimoConsignadoRepository.save(ocorrencia);
		emprestimoConsignadoRepository.save(emprestimoConsignado);
	}

}

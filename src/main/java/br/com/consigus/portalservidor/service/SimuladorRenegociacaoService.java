package br.com.consigus.portalservidor.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.dto.OfertaEmprestimo;
import br.com.consigus.portalservidor.entity.ConfiguracaoRenegociacaoEmprestimoConsignataria;
import br.com.consigus.portalservidor.entity.ProdutoConsignataria;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.enums.ProdutoConsignado;
import br.com.consigus.portalservidor.repository.ConfiguracaoRenegociacaoEmprestimoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ProdutoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ServidorRepository;

@Service
public class SimuladorRenegociacaoService {

	@Autowired
	private ServidorRepository servidorRepository;

	@Autowired
	private ProdutoConsignatariaRepository produtoConsignatariaRepository;

	@Autowired
	private ConfiguracaoRenegociacaoEmprestimoConsignatariaRepository configuracaoRenegociacaoEmprestimoConsignatariaRepository;

	public OfertaEmprestimo simularPorValorParcelas(Long idServidor, Double valor, Integer parcelas)
			throws ParseException {
		Servidor servidor = servidorRepository.findById(idServidor).get();
		if (servidor.getMargemEmprestimoDisponivel() == BigDecimal.ZERO) {
			return null;
		}
		ProdutoConsignataria produtoEmprestimo = produtoConsignatariaRepository
				.findByProduto(ProdutoConsignado.RENEGOCIACAO_EMPRESTIMO);
		ConfiguracaoRenegociacaoEmprestimoConsignataria configuracaoEmp = configuracaoRenegociacaoEmprestimoConsignatariaRepository
				.findByParcelasProduto_pk(parcelas, produtoEmprestimo.getPk());
		OfertaEmprestimo oferta = new OfertaEmprestimo();
		oferta.setDataSimulacao(LocalDate.now());
		oferta.setJurosMensal(configuracaoEmp.getJuros());
		oferta.setValorParcela(valor);
		oferta.setParcelas(configuracaoEmp.getParcelas());
		oferta.setValorEmprestimo(
				calcularValorFinanciado(valor, configuracaoEmp.getJuros(), configuracaoEmp.getParcelas()));
		oferta.setValorTotal(oferta.getValorParcela() * parcelas);
		return oferta;
	}

	public OfertaEmprestimo simularPorValorTotal(Long idServidor, Double valor, Integer parcelas)
			throws ParseException {
		Servidor servidor = servidorRepository.findById(idServidor).get();
		if (servidor.getMargemEmprestimoDisponivel() == BigDecimal.ZERO) {
			return null;
		}
		ProdutoConsignataria produtoEmprestimo = produtoConsignatariaRepository
				.findByProduto(ProdutoConsignado.RENEGOCIACAO_EMPRESTIMO);
		ConfiguracaoRenegociacaoEmprestimoConsignataria configuracaoEmp = configuracaoRenegociacaoEmprestimoConsignatariaRepository
				.findByParcelasProduto_pk(parcelas, produtoEmprestimo.getPk());
		OfertaEmprestimo oferta = new OfertaEmprestimo();
		oferta.setDataSimulacao(LocalDate.now());
		oferta.setJurosMensal(configuracaoEmp.getJuros());
		oferta.setValorEmprestimo(valor);
		oferta.setParcelas(configuracaoEmp.getParcelas());
		oferta.setValorParcela(calcularValorParcelas(valor, configuracaoEmp.getJuros(), configuracaoEmp.getParcelas()));
		oferta.setValorTotal(oferta.getValorParcela() * parcelas);
		return oferta;
	}

	private Double calcularValorParcelas(Double valor, double juros, Integer parcelas) throws ParseException {
		double jurosReal = juros / 100;
		double nc = (Math.pow((1 + jurosReal), parcelas) * jurosReal);
		double dc = (Math.pow((1 + jurosReal), parcelas) - 1);
		double c = nc / dc;
		double total = valor * c;
		String totalStr = new DecimalFormat("###0.000000").format(total);
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		Number number = format.parse(totalStr);
		return number.doubleValue();

	}

	private Double calcularValorFinanciado(Double valor, double juros, Integer parcelas) throws ParseException {
		double jurosReal = juros / 100;
		double nc = (Math.pow((1 + jurosReal), parcelas) * jurosReal);
		double dc = (Math.pow((1 + jurosReal), parcelas) - 1);
		double c = nc / dc;
		double total = valor / c;
		String totalStr = new DecimalFormat("###0.000000").format(total);
		NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
		Number number = format.parse(totalStr);
		return number.doubleValue();

	}

}

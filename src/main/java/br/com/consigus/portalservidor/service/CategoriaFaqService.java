package br.com.consigus.portalservidor.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.entity.CategoriaFaq;
import br.com.consigus.portalservidor.repository.CategoriaFaqRepository;

@Service
public class CategoriaFaqService implements ConsignusService<CategoriaFaq, Long> {

	@Autowired
	private CategoriaFaqRepository repository;

	@Override
	public CategoriaFaq findOne(Long pk) {
		return repository.findById(pk).get();
	}

	@Override
	public List<CategoriaFaq> findAll() {
		return repository.findAll();
	}

	@Override
	public List<CategoriaFaq> findAll(Map<String, String> params) {
		throw new RuntimeException("Ação não permitida.");
	}

	@Override
	public CategoriaFaq insert(CategoriaFaq entidade) {
		entidade.setAtivo(true);
		return repository.save(entidade);
	}

	@Override
	public CategoriaFaq patch(Long pk, CategoriaFaq entidade) {
		CategoriaFaq entidadeDb = this.findOne(pk);
		BeanUtils.copyProperties(entidade, entidadeDb, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
				"usuarioAlteracao", "dataHoraAlteracao");
		CategoriaFaq en = this.repository.save(entidadeDb);
		return en;
	}

	@Override
	public void delete(Long pk) {
		CategoriaFaq aviso = repository.findById(pk).get();
		aviso.setAtivo(false);
		repository.save(aviso);

	}

}

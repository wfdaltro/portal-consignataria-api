package br.com.consigus.portalservidor.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoCartaoConsignado;
import br.com.consigus.portalservidor.entity.CartaoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoCartaoConsignado;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrenciaCartaoConsignado;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.QCartaoConsignado;
import br.com.consigus.portalservidor.entity.QEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.QPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.enums.SituacaoCartao;
import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import br.com.consigus.portalservidor.enums.SituacaoProposta;
import br.com.consigus.portalservidor.repository.ArquivoDocumentoCartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.CartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.DocumentoCartaoConsignadoRespository;
import br.com.consigus.portalservidor.repository.OcorrrenciaCartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class CartaoConsignadoService {

	@Autowired
	private CartaoConsignadoRepository cartaoConsignadoRepository;

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Autowired
	private OcorrrenciaCartaoConsignadoRepository ocorrrenciaCartaoConsignadoRepository;

	@Autowired
	private DocumentoCartaoConsignadoRespository documentoCartaoConsignadoRespository;

	@Autowired
	private ArquivoDocumentoCartaoConsignadoRepository arquivoDocumentoCartaoConsignadoRepository;

	public List<CartaoConsignado> findAll(Map<String, String> params) {
		Iterable<CartaoConsignado> resultado = this.cartaoConsignadoRepository.findAll(this.getPredicate(params),
				Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QCartaoConsignado proposta = QCartaoConsignado.cartaoConsignado;
		BooleanExpression queryExpression = proposta.pk.isNotNull();
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		BooleanExpression consignataria = proposta.consignataria.pk.eq(entidadeConsignataria.getPk());
		queryExpression = queryExpression.and(consignataria);
		if (params.get("numero") != null && !params.get("numero").isBlank()) {
			BooleanExpression ade = proposta.ade.eq(params.get("numero"));
			queryExpression = queryExpression.and(ade);
		}
		
		if (params.get("servidor") != null) {
			BooleanExpression servidor = proposta.servidor.pk.eq(Long.valueOf(params.get("servidor")));
			queryExpression = queryExpression.and(servidor);
		}
		
		if (params.get("situacao") != null) {
			SituacaoCartao s = SituacaoCartao.get(params.get("situacao"));
			BooleanExpression situacao = proposta.situacao.eq(s);
			queryExpression = queryExpression.and(situacao);
		}
		return queryExpression;
	}

	public CartaoConsignado findOne(Long id) {
		return cartaoConsignadoRepository.findById(id).get();
	}

	public void ocorrencia(Long id, @Valid DadosOperacao dadosOperacao) {
		CartaoConsignado cartaoConsignado = cartaoConsignadoRepository.findById(id).get();
		OcorrenciaCartaoConsignado ocorrencia = new OcorrenciaCartaoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia(dadosOperacao.getObservacao());
		ocorrencia.setCartaoConsignado(cartaoConsignado);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());

		if (2 == dadosOperacao.getAcao()) { // Suspensa
			cartaoConsignado.setSituacao(SituacaoCartao.SUSPENSA);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.SUSPENSA.getDescricao());
		}
		if (3 == dadosOperacao.getAcao()) { // Liquidada
			cartaoConsignado.setSituacao(SituacaoCartao.CANCELADO);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.SUSPENSA.getDescricao());
		}

		ocorrrenciaCartaoConsignadoRepository.save(ocorrencia);
		cartaoConsignadoRepository.save(cartaoConsignado);

	}

	public void inserirAnexos(Long id, MultipartFile file, String nome) throws IOException {
		CartaoConsignado cartaoConsignado = cartaoConsignadoRepository.findById(id).get();
		DocumentoCartaoConsignado doc = new DocumentoCartaoConsignado();
		ArquivoDocumentoCartaoConsignado arquivo = new ArquivoDocumentoCartaoConsignado();
		doc.setDataCadastro(LocalDate.now());
		doc.setNome(nome);
		doc.setCartaoConsignado(cartaoConsignado);
		doc.setContentType(file.getContentType());
		doc.setTamanho(file.getSize());
		documentoCartaoConsignadoRespository.save(doc);
		arquivo.setArquivo(file.getBytes());
		arquivo.setDocumento(doc);
		arquivo.setContentType(file.getContentType());
		arquivoDocumentoCartaoConsignadoRepository.save(arquivo);

	}

	@Transactional
	public ArquivoDocumentoCartaoConsignado obterAnexo(Integer idAnexo) {
		return arquivoDocumentoCartaoConsignadoRepository.findByDocumento_pk(idAnexo).get();

	}

	@Transactional
	public Collection<DocumentoCartaoConsignado> anexos(Long id) {
		return documentoCartaoConsignadoRespository.findByCartaoConsignado_pk(id);
	}

	@Transactional
	public void removerArquivo(Integer id) {
		DocumentoCartaoConsignado DocumentoCartaoConsignado = documentoCartaoConsignadoRespository.findById(id).get();
		ArquivoDocumentoCartaoConsignado arquivo = arquivoDocumentoCartaoConsignadoRepository
				.findByDocumento_pk(DocumentoCartaoConsignado.getPk()).get();
		arquivoDocumentoCartaoConsignadoRepository.delete(arquivo);
		documentoCartaoConsignadoRespository.delete(DocumentoCartaoConsignado);
	}

}

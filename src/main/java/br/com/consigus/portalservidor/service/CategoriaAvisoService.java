package br.com.consigus.portalservidor.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.entity.CategoriaAviso;
import br.com.consigus.portalservidor.repository.CategoriaAvisoRepository;

@Service
public class CategoriaAvisoService implements ConsignusService<CategoriaAviso, Long> {

	@Autowired
	private CategoriaAvisoRepository repository;

	@Override
	public CategoriaAviso findOne(Long pk) {
		return repository.findById(pk).get();
	}

	@Override
	public List<CategoriaAviso> findAll() {
		return repository.findAll();
	}

	@Override
	public List<CategoriaAviso> findAll(Map<String, String> params) {
		throw new RuntimeException("Ação não permitida.");
	}

	@Override
	public CategoriaAviso insert(CategoriaAviso entidade) {
		entidade.setAtivo(true);
		return repository.save(entidade);
	}

	@Override
	public CategoriaAviso patch(Long pk, CategoriaAviso entidade) {
		CategoriaAviso entidadeDb = this.findOne(pk);
		BeanUtils.copyProperties(entidade, entidadeDb, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
				"usuarioAlteracao", "dataHoraAlteracao");
		CategoriaAviso en = this.repository.save(entidadeDb);
		return en;
	}

	@Override
	public void delete(Long pk) {
		CategoriaAviso aviso = repository.findById(pk).get();
		aviso.setAtivo(false);
		repository.save(aviso);

	}

}

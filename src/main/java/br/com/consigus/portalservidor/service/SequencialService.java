package br.com.consigus.portalservidor.service;

import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.entity.Sequencial;
import br.com.consigus.portalservidor.enums.TipoSequencial;
import br.com.consigus.portalservidor.repository.SequencialRepository;

@Service
public class SequencialService {

	@Autowired
	private SequencialRepository sequencialRepository;

	public String gerarNumeroProtocolo(TipoSequencial tipoSequencial) {
		Integer anoSistema = LocalDate.now().getYear();
		String ano;
		Integer sequencial;
		Sequencial seq = sequencialRepository.findForWrite(tipoSequencial.getCodigo());
		if (seq == null) {
			seq = new Sequencial();
			seq.setTipoSequencial(tipoSequencial.getCodigo());
			seq.setPk(tipoSequencial.getCodigo());
			sequencial = 1;
			ano = anoSistema.toString();
		} else {
			String ultimoSequencial = seq.getNumeroUltimoSequencial();
			ano = ultimoSequencial.substring(0, 4);
			Integer anoSequencial = Integer.valueOf(ano);
			if (!anoSistema.equals(anoSequencial)) {
				sequencial = 1;
				ano = anoSistema.toString();
			} else {
				sequencial = Integer.parseInt(ultimoSequencial.substring(4));
				sequencial++;
			}
		}
		seq.setNumeroUltimoSequencial(ano + StringUtils.leftPad(sequencial.toString(), 6, "0"));
		sequencialRepository.save(seq);
		return seq.getNumeroUltimoSequencial();
	}

}
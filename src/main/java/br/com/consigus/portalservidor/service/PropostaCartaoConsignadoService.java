package br.com.consigus.portalservidor.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoPropostaCartaoConsignado;
import br.com.consigus.portalservidor.entity.CartaoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoPropostaCartaoConsignado;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrrenciaPropostaCartaoConsignado;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.Parametros;
import br.com.consigus.portalservidor.entity.PropostaCartaoConsignado;
import br.com.consigus.portalservidor.entity.QPropostaCartaoConsignado;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.enums.SituacaoCartao;
import br.com.consigus.portalservidor.enums.SituacaoProposta;
import br.com.consigus.portalservidor.enums.TipoSequencial;
import br.com.consigus.portalservidor.mail.EmailHelper;
import br.com.consigus.portalservidor.repository.ArquivoDocumentoPropostaCartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.CartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.DocumentoPropostaCartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OcorrrenciaPropostaCartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ParametrosRepository;
import br.com.consigus.portalservidor.repository.PropostaCartaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.ServidorRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;
import br.com.consigus.portalservidor.utils.JasperHelper;

@Service
public class PropostaCartaoConsignadoService {

	@Autowired
	private PropostaCartaoConsignadoRepository propostaCartaoConsignadoRepository;

	@Autowired
	OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Autowired
	private ServidorRepository servidorRepository;

	@Autowired
	private SequencialService sequencialService;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private ParametrosRepository parametrosRepository;

	@Autowired
	private OcorrrenciaPropostaCartaoConsignadoRepository ocorrrenciaPropostaCartaoConsignadoRepository;

	@Autowired
	private CartaoConsignadoRepository cartaoConsignadoRepository;

	@Autowired
	private DocumentoPropostaCartaoConsignadoRepository documentoPropostaCartaoConsignadoRepository;

	@Autowired
	private ArquivoDocumentoPropostaCartaoConsignadoRepository arquivoDocumentoPropostaCartaoConsignadoRepository;
	
	@Autowired
	private JasperHelper helper;
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public PropostaCartaoConsignado insert(@Valid PropostaCartaoConsignado entidade)
			throws AddressException, MessagingException {
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		Servidor servidor = servidorRepository.findById(entidade.getServidor().getPk()).get();
		entidade.setOperadorResponsavel(operadorInclusao);
		entidade.setServidor(servidor);
		entidade.setConsignataria(entidadeConsignataria);
		entidade.setTokenLiberacao(gerarTokenProposta());
		entidade.setSituacao(SituacaoProposta.EM_ANALISE);
		Parametros parametros = parametrosRepository.findById(1).get();
		int diasValidadeProposta = 5;
		if (parametros != null && parametros.getPrazoMaximoAprovacaoContrato() != null) {
			diasValidadeProposta = parametros.getPrazoMaximoAprovacaoContrato();
		}
		entidade.setDataValidadeProposta(LocalDate.now().plusDays(diasValidadeProposta));
		entidade.setNumeroProposta(sequencialService.gerarNumeroProtocolo(TipoSequencial.PROPOSTA_CARTAO));
		PropostaCartaoConsignado propostaSalva = propostaCartaoConsignadoRepository.saveAndFlush(entidade);
		emailHelper.enviarEmail(servidor.getEmail(), "Proposta de emissão de cartão de crédito consignado.",
				"O código de validação da proposta número:  " + entidade.getNumeroProposta() + " é: "
						+ entidade.getTokenLiberacao());

		DadosOperacao dados = new DadosOperacao();
		dados.setObservacao("Proposta criada.");
		dados.setAcao(1);
		ocorrencia(entidade.getPk(),dados);
		
		servidor.setMargemCartaoDisponivel(new BigDecimal(0));
		servidor.setMargemCartaoReservada(servidor.getMargemEmprestimo());
		servidorRepository.save(servidor);

		return propostaSalva;
	}

	private String gerarTokenProposta() {
		return UUID.randomUUID().toString().substring(0, 6);
	}

	public List<PropostaCartaoConsignado> findAll(Map<String, String> params) {
		Iterable<PropostaCartaoConsignado> resultado = this.propostaCartaoConsignadoRepository
				.findAll(this.getPredicate(params), Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QPropostaCartaoConsignado proposta = QPropostaCartaoConsignado.propostaCartaoConsignado;
		BooleanExpression queryExpression = proposta.pk.isNotNull();
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		BooleanExpression consignataria = proposta.consignataria.pk.eq(entidadeConsignataria.getPk());
		queryExpression = queryExpression.and(consignataria);
		if (params.get("numero") != null && !params.get("numero").isBlank()) {
			BooleanExpression ade = proposta.numeroProposta.eq(params.get("numero"));
			queryExpression = queryExpression.and(ade);
		}
		
		if (params.get("servidor") != null) {
			BooleanExpression servidor = proposta.servidor.pk.eq(Long.valueOf(params.get("servidor")));
			queryExpression = queryExpression.and(servidor);
		}
		
		if (params.get("situacao") != null) {
			SituacaoProposta s = SituacaoProposta.get(params.get("situacao"));
			BooleanExpression situacao = proposta.situacao.eq(s);
			queryExpression = queryExpression.and(situacao);
		}
		return queryExpression;
	}

	public void ocorrencia(Long id, @Valid DadosOperacao dadosOperacao) {
		PropostaCartaoConsignado propostaCartaoConsignado = propostaCartaoConsignadoRepository.findById(id).get();
		OcorrrenciaPropostaCartaoConsignado ocorrencia = new OcorrrenciaPropostaCartaoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia(dadosOperacao.getObservacao());
		ocorrencia.setPropostaCartaoConsignado(propostaCartaoConsignado);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());

		if (3 == dadosOperacao.getAcao()) { // indeferir
			propostaCartaoConsignado.setSituacao(SituacaoProposta.INDEFERIDA);
			ocorrencia.setMotivoOcorrencia(SituacaoProposta.INDEFERIDA.getDescricao());
			Servidor servidor = servidorRepository.findById(propostaCartaoConsignado.getServidor().getPk()).get();
			servidor.setMargemCartaoDisponivel(servidor.getMargemEmprestimo());
			servidor.setMargemCartaoReservada(new BigDecimal(0));
			servidorRepository.save(servidor);

		}
		if (5 == dadosOperacao.getAcao()) { // devolver
			propostaCartaoConsignado.setSituacao(SituacaoProposta.DEVOLVIDA_AO_SOLICITANTE);
			ocorrencia.setMotivoOcorrencia(SituacaoProposta.DEVOLVIDA_AO_SOLICITANTE.getDescricao());
		}
		if (4 == dadosOperacao.getAcao()) { // cancelar
			propostaCartaoConsignado.setSituacao(SituacaoProposta.CANCELADA_PELO_SOLICITANTE);
			ocorrencia.setMotivoOcorrencia(SituacaoProposta.CANCELADA_PELO_SOLICITANTE.getDescricao());
			Servidor servidor = servidorRepository.findById(propostaCartaoConsignado.getServidor().getPk()).get();
			servidor.setMargemCartaoDisponivel(servidor.getMargemEmprestimo());
			servidor.setMargemCartaoReservada(new BigDecimal(0));
			servidorRepository.save(servidor);
		}
		if (1 == dadosOperacao.getAcao()) { // em análise
			propostaCartaoConsignado.setSituacao(SituacaoProposta.EM_ANALISE);
			ocorrencia.setMotivoOcorrencia(SituacaoProposta.EM_ANALISE.getDescricao());
		}
		ocorrrenciaPropostaCartaoConsignadoRepository.save(ocorrencia);
		propostaCartaoConsignadoRepository.save(propostaCartaoConsignado);

	}

	public PropostaCartaoConsignado findOne(Long id) {
		return propostaCartaoConsignadoRepository.findById(id).get();
	}

	public CartaoConsignado averbar(Long id, @Valid String codigoAverbacao) {
		PropostaCartaoConsignado proposta = propostaCartaoConsignadoRepository.findById(id).get();
		if (!proposta.getTokenLiberacao().equals(codigoAverbacao)) {
			throw new RuntimeException("O código de averbação informado não é válido.");
		}
		CartaoConsignado cartao = new CartaoConsignado();
		cartao.setConsignataria(proposta.getConsignataria());
		cartao.setAde(proposta.getNumeroProposta());
		cartao.setJurosAno(proposta.getJurosAno());
		cartao.setJurosMes(proposta.getJurosMes());
		cartao.setPercentualSaqueCartao(proposta.getPercentualSaqueCartao());
		cartao.setValorLimiteCartao(proposta.getValorLimiteCartao());
		cartao.setValorSaqueCartao(proposta.getValorSaqueCartao());
		cartao.setServidor(proposta.getServidor());
		cartao.setSituacao(SituacaoCartao.ATIVA);
		cartaoConsignadoRepository.save(cartao);
		gerarDocumentosCartao(proposta, cartao);
		proposta.setSituacao(SituacaoProposta.DEFERIDA);
		gravarOcorrenciaDeferimentoProposta(proposta);
		propostaCartaoConsignadoRepository.save(proposta);
		return cartao;
	}

	private void gravarOcorrenciaDeferimentoProposta(PropostaCartaoConsignado proposta) {
		OcorrrenciaPropostaCartaoConsignado ocorrencia = new OcorrrenciaPropostaCartaoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia("Proposta deferida");
		ocorrencia.setPropostaCartaoConsignado(proposta);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		ocorrencia.setMotivoOcorrencia(SituacaoProposta.DEFERIDA.getDescricao());
		ocorrrenciaPropostaCartaoConsignadoRepository.save(ocorrencia);
		
	}

	private void gerarDocumentosCartao(PropostaCartaoConsignado proposta, CartaoConsignado cartao) {
		 
		
	}
	
	public void inserirAnexos(Long idEmprestimo, MultipartFile file, String nome) throws IOException {
		PropostaCartaoConsignado consignado = propostaCartaoConsignadoRepository.findById(idEmprestimo).get();
		DocumentoPropostaCartaoConsignado doc = new DocumentoPropostaCartaoConsignado();
		ArquivoDocumentoPropostaCartaoConsignado arquivo = new ArquivoDocumentoPropostaCartaoConsignado();
		doc.setDataCadastro(LocalDate.now());
		doc.setNome(nome);
		doc.setPropostaCartaoConsignado(consignado);
		doc.setContentType(file.getContentType());
		doc.setTamanho(file.getSize());
		documentoPropostaCartaoConsignadoRepository.save(doc);
		arquivo.setArquivo(file.getBytes());
		arquivo.setDocumento(doc);
		arquivo.setContentType(file.getContentType());
		arquivoDocumentoPropostaCartaoConsignadoRepository.save(arquivo);

	}

	@Transactional
	public ArquivoDocumentoPropostaCartaoConsignado obterAnexo(Integer idAnexo) {
		return arquivoDocumentoPropostaCartaoConsignadoRepository.findById(idAnexo).get();

	}

	@Transactional
	public Collection<DocumentoPropostaCartaoConsignado> anexos(Long id) {
		return documentoPropostaCartaoConsignadoRepository.findByPropostaCartaoConsignado_pk(id);
	}

	@Transactional
	public void removerArquivo(Integer id) {
		DocumentoPropostaCartaoConsignado documentoConsignado = documentoPropostaCartaoConsignadoRepository
				.findById(id).get();
		ArquivoDocumentoPropostaCartaoConsignado arquivo = arquivoDocumentoPropostaCartaoConsignadoRepository
				.findByDocumento_pk(documentoConsignado.getPk()).get();
		arquivoDocumentoPropostaCartaoConsignadoRepository.delete(arquivo);
		documentoPropostaCartaoConsignadoRepository.delete(documentoConsignado);
	}

	public byte[] gerarRelatorioAutorizacaoDesconto(Long idProposta) {
		PropostaCartaoConsignado proposta = propostaCartaoConsignadoRepository.findById(idProposta).get();
		HashMap<String, Object> parameters = new HashMap<>();
		parameters.put("nomeServidor", proposta.getServidor().getNome());
		parameters.put("cpfServidor", proposta.getServidor().getCpf());
		parameters.put("consignataria", proposta.getConsignataria().getNome());

		return helper.pdf("termo_autorizacao_emprestimo", parameters, null);

	}

	

}

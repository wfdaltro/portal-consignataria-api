package br.com.consigus.portalservidor.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrrenciaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.ParcelasEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.QEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.QPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import br.com.consigus.portalservidor.enums.SituacaoProposta;
import br.com.consigus.portalservidor.repository.ArquivoDocumentoEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.DocumentoEmprestimoConsignadoRespository;
import br.com.consigus.portalservidor.repository.EmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OcorrrenciaEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ParcelasEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class EmprestimoConsignadoService {

	@Autowired
	private EmprestimoConsignadoRepository emprestimoConsignadoRepository;

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Autowired
	private ParcelasEmprestimoConsignadoRepository parcelasEmprestimoConsignadoRepository;

	@Autowired
	private OcorrrenciaEmprestimoConsignadoRepository ocorrrenciaEmprestimoConsignadoRepository;

	@Autowired
	private DocumentoEmprestimoConsignadoRespository documentoEmprestimoConsignadoRespository;

	@Autowired
	private ArquivoDocumentoEmprestimoConsignadoRepository arquivoDocumentoEmprestimoConsignadoRepository;

	public List<EmprestimoConsignado> findAll(Map<String, String> params) {
		Iterable<EmprestimoConsignado> resultado = this.emprestimoConsignadoRepository
				.findAll(this.getPredicate(params), Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QEmprestimoConsignado proposta = QEmprestimoConsignado.emprestimoConsignado;
		BooleanExpression queryExpression = proposta.pk.isNotNull();
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		BooleanExpression consignataria = proposta.consignataria.pk.eq(entidadeConsignataria.getPk());
		queryExpression = queryExpression.and(consignataria);
		if (params.get("numero") != null && !params.get("numero").isBlank()) {
			BooleanExpression ade = proposta.ade.eq(params.get("numero"));
			queryExpression = queryExpression.and(ade);
		}
		
		if (params.get("servidor") != null) {
			BooleanExpression servidor = proposta.servidor.pk.eq(Long.valueOf(params.get("servidor")));
			queryExpression = queryExpression.and(servidor);
		}
		
		if (params.get("situacao") != null) {
			SituacaoConsignacao s = SituacaoConsignacao.get(params.get("situacao"));
			BooleanExpression situacao = proposta.situacao.eq(s);
			queryExpression = queryExpression.and(situacao);
		}
		return queryExpression;
	}

	public EmprestimoConsignado findOne(Long id) {
		return emprestimoConsignadoRepository.findById(id).get();
	}

	public Collection<ParcelasEmprestimoConsignado> parcelas(Long id) {
		return parcelasEmprestimoConsignadoRepository.findByEmprestimoConsignado_pk(id);
	}

	public void ocorrencia(Long id, @Valid DadosOperacao dadosOperacao) {
		EmprestimoConsignado emprestimoConsignado = emprestimoConsignadoRepository.findById(id).get();
		OcorrrenciaEmprestimoConsignado ocorrencia = new OcorrrenciaEmprestimoConsignado();
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setDescricaoOcorrencia(dadosOperacao.getObservacao());
		ocorrencia.setEmprestimoConsignado(emprestimoConsignado);
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());

		if (1	 == dadosOperacao.getAcao()) { // Ativa
			emprestimoConsignado.setSituacao(SituacaoConsignacao.ATIVA);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.ATIVA.getDescricao());
		}
		if (2 == dadosOperacao.getAcao()) { // Suspensa
			emprestimoConsignado.setSituacao(SituacaoConsignacao.SUSPENSA);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.SUSPENSA.getDescricao());
		}
		if (3 == dadosOperacao.getAcao()) { // Liquidada
			emprestimoConsignado.setDataLiquidacao(dadosOperacao.getDataLiquidacao());
			emprestimoConsignado.setSituacao(SituacaoConsignacao.LIQUIDADA);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.LIQUIDADA.getDescricao());
		}
		if (4 == dadosOperacao.getAcao()) { // Consolidada
			emprestimoConsignado.setSituacao(SituacaoConsignacao.CONSOLIDADA);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.CONSOLIDADA.getDescricao());
		}
		if (5 == dadosOperacao.getAcao()) { // Portabilidade
			emprestimoConsignado.setSituacao(SituacaoConsignacao.COMPRADA);
			ocorrencia.setMotivoOcorrencia(SituacaoConsignacao.COMPRADA.getDescricao());
		}
		ocorrrenciaEmprestimoConsignadoRepository.save(ocorrencia);
		emprestimoConsignadoRepository.save(emprestimoConsignado);

	}

	@Transactional
	public void inserirAnexos(Long idEmprestimo, MultipartFile file, String nome) throws IOException {
		EmprestimoConsignado emprestimoConsignado = emprestimoConsignadoRepository.findById(idEmprestimo).get();
		DocumentoEmprestimoConsignado doc = new DocumentoEmprestimoConsignado();
		ArquivoDocumentoEmprestimoConsignado arquivo = new ArquivoDocumentoEmprestimoConsignado();
		doc.setDataCadastro(LocalDate.now());
		doc.setNome(nome);
		doc.setEmprestimoConsignado(emprestimoConsignado);
		doc.setContentType(file.getContentType());
		doc.setTamanho(file.getSize());
		documentoEmprestimoConsignadoRespository.save(doc);
		arquivo.setArquivo(file.getBytes());
		arquivo.setDocumento(doc);
		arquivoDocumentoEmprestimoConsignadoRepository.save(arquivo);

	}

	@Transactional
	public ArquivoDocumentoEmprestimoConsignado obterAnexo(Integer idAnexo) {
		return arquivoDocumentoEmprestimoConsignadoRepository.findByDocumento_pk(idAnexo).get();

	}

	@Transactional
	public Collection<DocumentoEmprestimoConsignado> anexos(Long id) {
		return documentoEmprestimoConsignadoRespository.findByEmprestimoConsignado_pk(id);
	}

	@Transactional
	public void removerArquivo(Integer id) {
		DocumentoEmprestimoConsignado documentoEmprestimoConsignado = documentoEmprestimoConsignadoRespository
				.findById(id).get();
		ArquivoDocumentoEmprestimoConsignado arquivo = arquivoDocumentoEmprestimoConsignadoRepository
				.findByDocumento_pk(documentoEmprestimoConsignado.getPk()).get();
		arquivoDocumentoEmprestimoConsignadoRepository.delete(arquivo);
		documentoEmprestimoConsignadoRespository.delete(documentoEmprestimoConsignado);
	}

	public Collection<EmprestimoConsignado> findByServidor(Long id) {
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria ec = operadorInclusao.getEntidadeConsignataria();
		return emprestimoConsignadoRepository.findByServidor_pkAndConsignataria_pkNot(id, ec.getPk());
	}
}

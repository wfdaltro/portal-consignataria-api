package br.com.consigus.portalservidor.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrenciaOperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.QOperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.enums.SituacaoUsuario;
import br.com.consigus.portalservidor.mail.Email;
import br.com.consigus.portalservidor.mail.EmailHelper;
import br.com.consigus.portalservidor.repository.EntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OcorrenciaOperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.security.GeradorSenha;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;
import br.com.consigus.portalservidor.security.UsuarioAutenticado;

@Service
public class OperadorConsignatariaService implements ConsignusService<OperadorEntidadeConsignataria, Long> {

	@Autowired
	private OperadorEntidadeConsignatariaRepository repository;

	@Autowired
	private EntidadeConsignatariaRepository entidadeConsignatariaRepository;

	@Autowired
	private OcorrenciaOperadorEntidadeConsignatariaRepository ocorrenciaOperadorEntidadeConsignatariaRepository;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private GeradorSenha geradorSenha;

	public OperadorEntidadeConsignataria findOne(Long pk) {
		return this.repository.findById(pk).get();
	}

	@Override
	public List<OperadorEntidadeConsignataria> findAll() {
		throw new RuntimeException("Ação não permitida.");
	}

	@Override
	public List<OperadorEntidadeConsignataria> findAll(Map<String, String> params) {
		Iterable<OperadorEntidadeConsignataria> resultado = this.repository.findAll(this.getPredicate(params),
				Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	@Override
	public OperadorEntidadeConsignataria insert(OperadorEntidadeConsignataria entidade) {
		EntidadeConsignataria entidadeConsignataria = repository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado()).getEntidadeConsignataria();
		entidade.setSituacao(SituacaoUsuario.ATIVO);
		entidade.setEntidadeConsignataria(entidadeConsignataria);
		String senha = entidade.getNome().trim().substring(0,1) + entidade.getCpf().substring(0,3);
		entidade.setSenha(new BCryptPasswordEncoder().encode(senha));
		entidade.setLogin(entidade.getCpf());
		OperadorEntidadeConsignataria en = this.repository.saveAndFlush(entidade);
		enviarEmailNovoUsuario(entidade.getLogin(), senha, entidade.getEmail());
		return en;
	}

	private void enviarEmailNovoUsuario(String login, String senha, String email) {
		try {
			String mensagem = Email.CADASTRO_NOVO_USUARIO.getMensagem();
			mensagem = mensagem.replace("#{login}", login).replace("#{senha}", senha);
			emailHelper.enviarEmail(email, "Seu acesso foi liberado no Consignus", mensagem);
		} catch (MessagingException e) {
			// TODO Auto-generated catch blockß
			e.printStackTrace();
		}

	}

	@Override
	public OperadorEntidadeConsignataria patch(Long pk, OperadorEntidadeConsignataria entidade) {
		OperadorEntidadeConsignataria entidadeDb = this.findOne(pk);
		BeanUtils.copyProperties(entidade, entidadeDb, "pk", "situacao", "versao", "usuarioInclusao",
				"dataHoraInclusao", "usuarioAlteracao", "dataHoraAlteracao", "entidadeConsignataria");
		OperadorEntidadeConsignataria en = this.repository.save(entidadeDb);
		return en;
	}

	@Override
	public void delete(Long pk) {
		throw new java.lang.SecurityException("Operação não permitida");

	}

	public void bloquear(Long id, OcorrenciaOperadorEntidadeConsignataria ocorrencia) {
		this.gerarOcorrencia(id, ocorrencia, "BLOQUEIO");
	}

	public void desbloquear(Long id, OcorrenciaOperadorEntidadeConsignataria ocorrencia) {
		this.gerarOcorrencia(id, ocorrencia, "DESBLOQUEIO");
	}

	private void gerarOcorrencia(Long id, OcorrenciaOperadorEntidadeConsignataria ocorrencia, String acao) {
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		ocorrencia.setStatusGeradoOcorrencia(acao);
		OperadorEntidadeConsignataria operadorEntidadeConsignataria = repository.findById(id).get();
		ocorrencia.setOperadorEntidadeConsignataria(operadorEntidadeConsignataria);
		ocorrenciaOperadorEntidadeConsignatariaRepository.save(ocorrencia);
		SituacaoUsuario situacao = SituacaoUsuario.BLOQUEADO;
		if ("DESBLOQUEIO".equalsIgnoreCase(acao)) {
			situacao = SituacaoUsuario.ATIVO;
		}
		operadorEntidadeConsignataria.setSituacao(situacao);
		this.repository.save(operadorEntidadeConsignataria);
	}

	 
	protected BooleanExpression getPredicate(Map<String, String> params) {
		QOperadorEntidadeConsignataria operadorEntidadeConsignataria = QOperadorEntidadeConsignataria.operadorEntidadeConsignataria;
		BooleanExpression queryExpression = operadorEntidadeConsignataria.pk.isNotNull();
		String login = JwtUsuarioAutenticado.getLoginUsuarioAutenticado();
		Long idConsignataria = repository.findByLogin(login).getEntidadeConsignataria().getPk();
		BooleanExpression entidadeConsignataria = operadorEntidadeConsignataria.entidadeConsignataria.pk
				.eq(idConsignataria);
		queryExpression = queryExpression.and(entidadeConsignataria);
		
		if (params.get("cpf") != null) {
			BooleanExpression cpf = operadorEntidadeConsignataria.cpf.eq(params.get("cpf"));
			queryExpression = queryExpression.and(cpf);
		}
		if (params.get("nome") != null) {
			BooleanExpression nome = operadorEntidadeConsignataria.nome.likeIgnoreCase("%" + params.get("nome") + "%");
			queryExpression = queryExpression.and(nome);
		}
		return queryExpression;
	}
	
	
	

	public UsuarioAutenticado obterDadosUsuarioAutenticado() {
		String login = JwtUsuarioAutenticado.getLoginUsuarioAutenticado();
		OperadorEntidadeConsignataria usuario = repository.findByLogin(login);
		return new UsuarioAutenticado(usuario.getPk(), usuario.getLogin(), usuario.getMatricula(), usuario.getNome(),
				usuario.getSituacao(), false, usuario.getSenha(), usuario.isMaster());
	}

}

package br.com.consigus.portalservidor.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OcorrenciaEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.QEntidadeConsignataria;
import br.com.consigus.portalservidor.enums.SituacaoEntidadeConsignataria;
import br.com.consigus.portalservidor.repository.EntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OcorrenciaEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class EntidadeConsignatariaService implements ConsignusService<EntidadeConsignataria, Long> {

	@Autowired
	private EntidadeConsignatariaRepository repository;

	@Autowired
	private OcorrenciaEntidadeConsignatariaRepository ocorrenciaEntidadeConsignatariaRepository;

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Override
	public EntidadeConsignataria findOne(Long pk) {
		return this.repository.findById(pk).get();
	}

	@Override
	public List<EntidadeConsignataria> findAll() {
		return this.repository.findAll(Sort.by(Direction.DESC, "pk"));
	}

	@Override
	public List<EntidadeConsignataria> findAll(Map<String, String> params) {
		Iterable<EntidadeConsignataria> resultado = this.repository.findAll(this.getPredicate(params),
				Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	@Override
	public EntidadeConsignataria insert(EntidadeConsignataria entidade) {
		entidade.setSituacao(SituacaoEntidadeConsignataria.ATIVO);
		EntidadeConsignataria en = this.repository.saveAndFlush(entidade);
		return en;
	}

	@Override
	public EntidadeConsignataria patch(Long pk, EntidadeConsignataria entidade) {
		EntidadeConsignataria entidadeDb = this.findOne(pk);
		BeanUtils.copyProperties(entidade, entidadeDb, "pk", "situacao", "versao", "usuarioInclusao",
				"dataHoraInclusao", "usuarioAlteracao", "dataHoraAlteracao");
		EntidadeConsignataria en = this.repository.save(entidadeDb);
		return en;
	}

	@Override
	public void delete(Long pk) {
		throw new java.lang.SecurityException("Operação não permitida");

	}

	public List<OperadorEntidadeConsignataria> findOperadores(Long id) {
		return operadorEntidadeConsignatariaRepository.findByEntidadeConsignataria_pk(id);
	}

	public void bloquear(Long id, OcorrenciaEntidadeConsignataria ocorrencia) {
		this.gerarOcorrencia(id, ocorrencia, "BLOQUEIO");
	}

	public void desbloquear(Long id, OcorrenciaEntidadeConsignataria ocorrencia) {
		this.gerarOcorrencia(id, ocorrencia, "DESBLOQUEIO");
	}

	private void gerarOcorrencia(Long id, OcorrenciaEntidadeConsignataria ocorrencia, String acao) {
		ocorrencia.setDataHoraOcorrencia(LocalDateTime.now());
		ocorrencia.setUsuarioOcorrencia(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		ocorrencia.setStatusGeradoOcorrencia(acao);
		EntidadeConsignataria entidadeConsignataria = repository.findById(id).get();
		ocorrencia.setEntidadeConsignataria(entidadeConsignataria);
		ocorrenciaEntidadeConsignatariaRepository.save(ocorrencia);
		SituacaoEntidadeConsignataria situacao = SituacaoEntidadeConsignataria.BLOQUEADO;
		if ("DESBLOQUEIO".equalsIgnoreCase(acao)) {
			situacao = SituacaoEntidadeConsignataria.ATIVO;
		}
		entidadeConsignataria.setSituacao(situacao);
		this.repository.save(entidadeConsignataria);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QEntidadeConsignataria entidadeConsignataria = QEntidadeConsignataria.entidadeConsignataria;
		BooleanExpression queryExpression = entidadeConsignataria.pk.isNotNull();
		if (params.get("nome") != null) {
			BooleanExpression nome = entidadeConsignataria.nome.likeIgnoreCase("%" + params.get("nome") + "%");
			BooleanExpression nomeFantasia = entidadeConsignataria.nomeFantasia
					.likeIgnoreCase("%" + params.get("nome") + "%");
			queryExpression = queryExpression.and((nome).or(nomeFantasia));
		}
		if (params.get("cnpj") != null) {
			BooleanExpression matricula = entidadeConsignataria.cnpj.eq(params.get("cnpj"));
			queryExpression = queryExpression.and(matricula);
		}
		if (params.get("situacao") != null) {
			BooleanExpression cpf = entidadeConsignataria.situacao
					.eq(SituacaoEntidadeConsignataria.get(params.get("situacao")));
			queryExpression = queryExpression.and(cpf);
		}
		return queryExpression;
	}

	public EntidadeConsignataria get() {
		OperadorEntidadeConsignataria op = operadorEntidadeConsignatariaRepository.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		return repository.findById(op.getEntidadeConsignataria().getPk()).get();
	}

}

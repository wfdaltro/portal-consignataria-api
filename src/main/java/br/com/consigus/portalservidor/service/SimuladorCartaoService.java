package br.com.consigus.portalservidor.service;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.dto.OfertaCartao;
import br.com.consigus.portalservidor.entity.ConfiguracaoCartaoConsignataria;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.ProdutoConsignataria;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.enums.ProdutoConsignado;
import br.com.consigus.portalservidor.repository.ConfiguracaoCartaoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ProdutoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ServidorRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class SimuladorCartaoService {

	@Autowired
	private ServidorRepository servidorRepository;

	@Autowired
	private ProdutoConsignatariaRepository produtoConsignatariaRepository;

	@Autowired
	private ConfiguracaoCartaoConsignatariaRepository configuracaoCartaoConsignatariaRepository;
	
	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	public OfertaCartao obterOferta(Long idServidor) {
		Servidor servidor = servidorRepository.findById(idServidor).get();
		if (servidor.getMargemCartaoDisponivel() == BigDecimal.ZERO) {
			return null;
		}
		EntidadeConsignataria entidadeConsignataria = operadorEntidadeConsignatariaRepository.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado()).getEntidadeConsignataria();
		ProdutoConsignataria produtocartao = produtoConsignatariaRepository.findByProdutoAndEntidadeConsignataria(ProdutoConsignado.CARTAO, entidadeConsignataria);
		if (produtocartao == null) {
			throw new RuntimeException("Consignatária não possui produto cadastrado");
		}
		ConfiguracaoCartaoConsignataria cartao = configuracaoCartaoConsignatariaRepository
				.findByProduto_pk(produtocartao.getPk()).get();
		if (cartao == null) {
			throw new RuntimeException("Não existem propostas disponíveis.");
		}
		BigDecimal limite = servidor.getMargemCartaoDisponivel()
				.multiply(new BigDecimal(cartao.getMultipladorLimiteCartao()));
		Double percentualSaque = cartao.getPercentualSaqueCartao();
		BigDecimal valorSaqueAutorizado = limite.multiply(new BigDecimal(percentualSaque / 100));
		Double juros = cartao.getJuros();
		Double jurosAnual = cartao.getJurosAnual();
		return new OfertaCartao(limite, percentualSaque, valorSaqueAutorizado, juros, jurosAnual, LocalDate.now());
	}

	 

}

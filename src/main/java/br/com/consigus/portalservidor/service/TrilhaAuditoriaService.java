package br.com.consigus.portalservidor.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.entity.EntidadeAuditavel;
import br.com.consigus.portalservidor.entity.QServidor;
import br.com.consigus.portalservidor.entity.QTrilhaAuditoria;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.entity.TrilhaAuditoria;
import br.com.consigus.portalservidor.enums.TipoOperacaoSistema;
import br.com.consigus.portalservidor.repository.TrilhaAuditoriaRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class TrilhaAuditoriaService {

	@Autowired
	private TrilhaAuditoriaRepository trilhaAuditoriaRepository;

	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(TipoOperacaoSistema operacao, EntidadeAuditavel entidadeOriginal,
			EntidadeAuditavel entidadeAlterada) throws JsonProcessingException {
		String nomeTabela = entidadeAlterada.getClass().getAnnotation(Table.class).name();
		String valorAntesAtualizacaoJson = "";
		if (entidadeOriginal != null) {
			valorAntesAtualizacaoJson = new ObjectMapper().writeValueAsString(entidadeOriginal);
		}
		String valorDepoisAtualizacaoJson = new ObjectMapper().writeValueAsString(entidadeAlterada);
		TrilhaAuditoria trilha = new TrilhaAuditoria();
		trilha.setTabela(nomeTabela);
		if (entidadeOriginal != null) {
			trilha.setTipoOperacaoSistema(TipoOperacaoSistema.ALTERACAO);
		}else {
			trilha.setTipoOperacaoSistema(TipoOperacaoSistema.INCLUSAO);
		}
		trilha.setValorAntesAtualizacao(valorAntesAtualizacaoJson);
		trilha.setValorAposAtualizacao(valorDepoisAtualizacaoJson);
		trilha.setUsuarioOperacao(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		this.trilhaAuditoriaRepository.saveAndFlush(trilha);
	}

	public List<TrilhaAuditoria> listar() {
		return trilhaAuditoriaRepository.findAll();
	}
	
	public  TrilhaAuditoria  detalhar(Long id) {
		return trilhaAuditoriaRepository.findById(id).get();
	}

	public List<TrilhaAuditoria> findAll(Map<String, String> params) {
		Iterable<TrilhaAuditoria> resultado = this.trilhaAuditoriaRepository.findAll(this.getPredicate(params),
				Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}
	
	protected BooleanExpression getPredicate(Map<String, String> params) {
		QTrilhaAuditoria trilhaAuditoria = QTrilhaAuditoria.trilhaAuditoria;
		BooleanExpression queryExpression = trilhaAuditoria.pk.isNotNull();
		if (params.get("matricula") != null) {
			BooleanExpression nome = trilhaAuditoria.usuarioOperacao.eq(params.get("matricula"));
			queryExpression = queryExpression.and(nome);
		}
		if (params.get("tipoOperacao") != null) {
			BooleanExpression matricula = trilhaAuditoria.tipoOperacaoSistema.eq(TipoOperacaoSistema.get(params.get("tipoOperacao")));
			queryExpression = queryExpression.and(matricula);
		}
		return queryExpression;
	}

}

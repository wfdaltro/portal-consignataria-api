package br.com.consigus.portalservidor.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.entity.QServidor;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.repository.ServidorRepository;

@Service
public class ServidorService {

	@Autowired
	private ServidorRepository repository;

	public Servidor findOne(Long pk) {
		return this.repository.findById(pk).get();
	}

	public Servidor findByCpf(String cpf) {
		return this.repository.findByCpf(cpf).get();
	}

	public List<Servidor> findAll(Map<String, String> params) {
		Iterable<Servidor> resultado = this.repository.findAll(this.getPredicate(params),
				Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QServidor servidor = QServidor.servidor;
		BooleanExpression queryExpression = servidor.pk.isNotNull();
		if (params.get("nome") != null) {
			BooleanExpression nome = servidor.nome.likeIgnoreCase("%" + params.get("nome") + "%");
			queryExpression = queryExpression.and(nome);
		}
		if (params.get("matricula") != null) {
			BooleanExpression matricula = servidor.matricula.eq(params.get("matricula"));
			queryExpression = queryExpression.and(matricula);
		}
		if (params.get("cpf") != null) {
			BooleanExpression cpf = servidor.cpf.eq(params.get("cpf"));
			queryExpression = queryExpression.and(cpf);
		}
		if (params.get("cargo") != null) {
			BooleanExpression cargo = servidor.cargo.likeIgnoreCase("%" + params.get("cargo") + "%");
			queryExpression = queryExpression.and(cargo);
		}
		if (params.get("lotacao") != null) {
			BooleanExpression lotacao = servidor.lotacao.likeIgnoreCase("%" + params.get("lotacao") + "%");
			queryExpression = queryExpression.and(lotacao);
		}

		return queryExpression;
	}

	public Servidor findByCpfMatricula(String chave) {
		return this.repository.findByCpfMatricula(chave);
	}

}

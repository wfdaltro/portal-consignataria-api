package br.com.consigus.portalservidor.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.consigus.portalservidor.entity.Parametros;
import br.com.consigus.portalservidor.repository.ParametrosRepository;

@Service
public class ParametrosService implements ConsignusService<Parametros, Integer> {

	@Autowired
	private ParametrosRepository parametrosRepository;

	@Override
	public Parametros findOne(Integer pk) {
		return parametrosRepository.findById(pk).get();
	}

	@Override
	public List<Parametros> findAll() {
		throw new RuntimeException("Ação não permitida");
	}

	@Override
	public List<Parametros> findAll(Map<String, String> params) {
		throw new RuntimeException("Ação não permitida");
	}

	@Override
	public Parametros insert(Parametros entidade) {
		Parametros en = this.parametrosRepository.saveAndFlush(entidade);
		return en;
	}

	@Override
	public Parametros patch(Integer pk, Parametros entidade) {
		Parametros entidadeDb = this.parametrosRepository.findById(pk).get();
		BeanUtils.copyProperties(entidade, entidadeDb, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
				"usuarioAlteracao", "dataHoraAlteracao");
		Parametros en = this.parametrosRepository.save(entidadeDb);
		return en;
	}

	@Override
	public void delete(Integer pk) {
		throw new RuntimeException("Ação não permitida");

	}

}

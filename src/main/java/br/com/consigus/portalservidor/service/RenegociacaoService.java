package br.com.consigus.portalservidor.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.ImmutableList;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.entity.Parametros;
import br.com.consigus.portalservidor.entity.ParcelasEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.PropostaRenegociacaoConsignado;
import br.com.consigus.portalservidor.entity.QPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import br.com.consigus.portalservidor.enums.SituacaoProposta;
import br.com.consigus.portalservidor.enums.TipoSequencial;
import br.com.consigus.portalservidor.mail.EmailHelper;
import br.com.consigus.portalservidor.repository.EmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ParametrosRepository;
import br.com.consigus.portalservidor.repository.ParcelasEmprestimoConsignadoRepository;
import br.com.consigus.portalservidor.repository.PropostaRenegociacaoConsignadoRepository;
import br.com.consigus.portalservidor.repository.ServidorRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

@Service
public class RenegociacaoService {

	@Autowired
	private PropostaRenegociacaoConsignadoRepository propostaRenegociacaoConsignadoRepository;

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Autowired
	private ServidorRepository servidorRepository;

	@Autowired
	private SequencialService sequencialService;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private ParametrosRepository parametrosRepository;

	@Autowired
	private EmprestimoConsignadoRepository emprestimoConsignadoRepository;

	@Autowired
	private ParcelasEmprestimoConsignadoRepository parcelasEmprestimoConsignadoRepository;

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public PropostaRenegociacaoConsignado insert(@Valid PropostaRenegociacaoConsignado entidade)
			throws AddressException, MessagingException {
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		Servidor servidor = servidorRepository.findById(entidade.getServidor().getPk()).get();
		entidade.setOperadorResponsavel(operadorInclusao);
		entidade.setServidor(servidor);
		entidade.setSituacao(SituacaoProposta.EM_ANALISE);
		entidade.setConsignataria(entidadeConsignataria);
		entidade.setTokenLiberacao(gerarTokenProposta());
		entidade.setNumeroProposta(sequencialService.gerarNumeroProtocolo(TipoSequencial.PROPOSTA_EMPRESTIMO));
		Parametros parametros = parametrosRepository.findById(1).get();
		int diasValidadeProposta = 5;
		if (parametros != null && parametros.getPrazoMaximoAprovacaoContrato() != null) {
			diasValidadeProposta = parametros.getPrazoMaximoAprovacaoContrato();
		}
		entidade.setDataValidadeProposta(LocalDate.now().plusDays(diasValidadeProposta));
		PropostaRenegociacaoConsignado propostaSalva = propostaRenegociacaoConsignadoRepository.saveAndFlush(entidade);
		
		try {
			emailHelper.enviarEmail(servidor.getEmail(), "Proposta de emissão de empréstimo  consignado.",
					"O código de validação da proposta número:  " + entidade.getNumeroProposta() + " é: "
							+ entidade.getTokenLiberacao());
			servidor.setMargemEmprestimoReservada(
					servidor.getMargemEmprestimoReservada().add(entidade.getValorPrestacao()));
			servidor.setMargemEmprestimoDisponivel(
					servidor.getMargemEmprestimoDisponivel().subtract(servidor.getMargemEmprestimoReservada()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		servidorRepository.save(servidor);
		DadosOperacao dados = new DadosOperacao();
		dados.setObservacao("Proposta criada.");
		return propostaSalva;
	}

	private String gerarTokenProposta() {
		return UUID.randomUUID().toString().substring(0, 6);
	}

	public EmprestimoConsignado averbar(Long id, String codigoAverbacao) {
		PropostaRenegociacaoConsignado proposta = propostaRenegociacaoConsignadoRepository.findById(id).get();
		if (!proposta.getTokenLiberacao().equals(codigoAverbacao)) {
			throw new RuntimeException("O código de averbação informado não é válido.");
		}
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EmprestimoConsignado emprestimo = new EmprestimoConsignado();
		emprestimo.setConsignataria(proposta.getConsignataria());
		emprestimo.setAde(proposta.getNumeroProposta());
		emprestimo.setJurosAoMes(proposta.getJurosMes());
		emprestimo.setParcelasEmAberto(proposta.getParcelas());
		emprestimo.setServidor(proposta.getServidor());
		emprestimo.setSituacao(SituacaoConsignacao.ATIVA);
		emprestimo.setTotalSaldoResidual(proposta.getValorLiberado());
		emprestimo.setTotalDeParcelas(proposta.getParcelas());
		emprestimo.setValorFinanciado(proposta.getValorLiberado());
		emprestimo.setValorPrestacao(proposta.getValorPrestacao());
		emprestimo.setValorTotalFinanciamento(proposta.getValorTotal());
		emprestimoConsignadoRepository.save(emprestimo);
		gerarParcelasEmpretimo(emprestimo);
		proposta.setSituacao(SituacaoProposta.DEFERIDA);
		propostaRenegociacaoConsignadoRepository.save(proposta);
		return emprestimo;
	}

	private void gerarParcelasEmpretimo(EmprestimoConsignado emprestimo) {
		List<ParcelasEmprestimoConsignado> parcelas = new ArrayList<>();
		for (int i = 0; i < emprestimo.getTotalDeParcelas(); i++) {
			ParcelasEmprestimoConsignado parcela = new ParcelasEmprestimoConsignado();
			parcela.setNumero(i);
			parcela.setPaga(false);
			parcela.setValorPrincipal(emprestimo.getValorPrestacao());
			parcela.setDataVencimento(LocalDate.now().plusMonths((i + 1)));
			parcela.setEmprestimoConsignado(emprestimo);
			parcelas.add(parcela);
		}
		parcelasEmprestimoConsignadoRepository.saveAll(parcelas);
	}

	public List<PropostaRenegociacaoConsignado> findAll(Map<String, String> params) {
		Iterable<PropostaRenegociacaoConsignado> resultado = this.propostaRenegociacaoConsignadoRepository
				.findAll(this.getPredicate(params), Sort.by(Direction.DESC, "pk"));
		return ImmutableList.copyOf(resultado);
	}

	protected BooleanExpression getPredicate(Map<String, String> params) {
		QPropostaEmprestimoConsignado proposta = QPropostaEmprestimoConsignado.propostaEmprestimoConsignado;
		BooleanExpression queryExpression = proposta.pk.isNotNull();
		OperadorEntidadeConsignataria operadorInclusao = operadorEntidadeConsignatariaRepository
				.findByLogin(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
		EntidadeConsignataria entidadeConsignataria = operadorInclusao.getEntidadeConsignataria();
		BooleanExpression consignataria = proposta.consignataria.pk.eq(entidadeConsignataria.getPk());
		queryExpression = queryExpression.and(consignataria);
		if (params.get("numero") != null && !params.get("numero").isBlank()) {
			BooleanExpression ade = proposta.numeroProposta.eq(params.get("numero"));
			queryExpression = queryExpression.and(ade);
		}

		if (params.get("servidor") != null) {
			BooleanExpression servidor = proposta.servidor.pk.eq(Long.valueOf(params.get("servidor")));
			queryExpression = queryExpression.and(servidor);
		}

		if (params.get("situacao") != null) {
			SituacaoProposta s = SituacaoProposta.get(params.get("situacao"));
			BooleanExpression situacao = proposta.situacao.eq(s);
			queryExpression = queryExpression.and(situacao);
		}
		return queryExpression;
	}

	public PropostaRenegociacaoConsignado findOne(Long id) {
		return propostaRenegociacaoConsignadoRepository.findById(id).get();
	}

}

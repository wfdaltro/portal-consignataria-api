package br.com.consigus.portalservidor.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.consigus.portalservidor.entity.ConfiguracaoCartaoConsignataria;
import br.com.consigus.portalservidor.entity.ConfiguracaoEmprestimoConsignataria;
import br.com.consigus.portalservidor.entity.ConfiguracaoRenegociacaoEmprestimoConsignataria;
import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.entity.ProdutoConsignataria;
import br.com.consigus.portalservidor.enums.ProdutoConsignado;
import br.com.consigus.portalservidor.repository.ConfiguracaoCartaoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ConfiguracaoEmprestimoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ConfiguracaoRenegociacaoEmprestimoConsignatariaRepository;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;
import br.com.consigus.portalservidor.repository.ProdutoConsignatariaRepository;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;
import io.github.millij.poi.SpreadsheetReadException;
import io.github.millij.poi.ss.reader.XlsxReader;

@Service
public class ProdutoConsignatariaService {

	@Autowired
	private ProdutoConsignatariaRepository produtoConsignatariaRepository;

	@Autowired
	private ConfiguracaoCartaoConsignatariaRepository configuracaoCartaoConsignatariaRepository;

	@Autowired
	private ConfiguracaoEmprestimoConsignatariaRepository configuracaoEmprestimoConsignatariaRepository;

	@Autowired
	private ConfiguracaoRenegociacaoEmprestimoConsignatariaRepository configuracaoRenegociacaoEmprestimoConsignatariaRepository;

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	public ProdutoConsignataria findOne(Long pk) {
		return this.produtoConsignatariaRepository.findById(pk).get();
	}

	public List<ProdutoConsignataria> findAll() {
		EntidadeConsignataria consignataria = getConsignataria();
		return this.produtoConsignatariaRepository.findByEntidadeConsignataria_pk(consignataria.getPk(),
				Sort.by(Direction.DESC, "pk"));
	}

	public ConfiguracaoCartaoConsignataria insertCartao(ConfiguracaoCartaoConsignataria cartao) {
		ProdutoConsignataria produto = createproduto();
		produto.setProduto(ProdutoConsignado.CARTAO);
		produtoConsignatariaRepository.save(produto);
		cartao.setProduto(produto);
		ConfiguracaoCartaoConsignataria ccec = this.configuracaoCartaoConsignatariaRepository.saveAndFlush(cartao);
		return ccec;
	}

	public List<ConfiguracaoEmprestimoConsignataria> insertEmprestimo(
			List<ConfiguracaoEmprestimoConsignataria> emprestimos) {
		ProdutoConsignataria produto = createproduto();
		produto.setProduto(ProdutoConsignado.EMPRESTIMO);
		produtoConsignatariaRepository.save(produto);
		for (ConfiguracaoEmprestimoConsignataria emprestimo : emprestimos) {
			emprestimo.setProduto(produto);
		}
		List<ConfiguracaoEmprestimoConsignataria> salvos = this.configuracaoEmprestimoConsignatariaRepository
				.saveAll(emprestimos);
		return salvos;
	}

	private ProdutoConsignataria createproduto() {
		ProdutoConsignataria produto = new ProdutoConsignataria();
		produto.setEntidadeConsignataria(getConsignataria());
		produto.setAtivo(true);
		return produto;
	}

	private EntidadeConsignataria getConsignataria() {
		String login = JwtUsuarioAutenticado.getLoginUsuarioAutenticado();
		EntidadeConsignataria consignataria = operadorEntidadeConsignatariaRepository.findByLogin(login)
				.getEntidadeConsignataria();
		return consignataria;
	}

	public List<ConfiguracaoEmprestimoConsignataria> parserArquivoParcelas(MultipartFile file)
			throws SpreadsheetReadException, IOException {
		final XlsxReader reader = new XlsxReader();
		return reader.read(ConfiguracaoEmprestimoConsignataria.class, file.getInputStream());
	}

	public ConfiguracaoCartaoConsignataria findCartao(Long pk) {
		return this.configuracaoCartaoConsignatariaRepository.findByProduto_pk(pk).get();
	}

	public List<ConfiguracaoEmprestimoConsignataria> findEmprestimo(Long pk) {
		return this.configuracaoEmprestimoConsignatariaRepository.findByProduto_pk(pk);
	}

	public ConfiguracaoCartaoConsignataria atualizarCartao(ConfiguracaoCartaoConsignataria cartao) {
		ConfiguracaoCartaoConsignataria entidadeDb = this.configuracaoCartaoConsignatariaRepository
				.findById(cartao.getPk()).get();
		BeanUtils.copyProperties(cartao, entidadeDb, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
				"usuarioAlteracao", "dataHoraAlteracao", "produto");
		ConfiguracaoCartaoConsignataria en = this.configuracaoCartaoConsignatariaRepository.save(entidadeDb);
		return en;
	}

	public void atualizarEmprestimos(Long pk, List<ConfiguracaoEmprestimoConsignataria> emprestimos) {
		ProdutoConsignataria produtoConsignataria = produtoConsignatariaRepository.findById(pk).get();
		List<ConfiguracaoEmprestimoConsignataria> configuracoes = configuracaoEmprestimoConsignatariaRepository
				.findByProduto_pk(pk);
		List<ConfiguracaoEmprestimoConsignataria> configuracoesIncluir = separarEmprestimosInclusao(
				produtoConsignataria, emprestimos, configuracoes);
		List<ConfiguracaoEmprestimoConsignataria> configuracoesAlterar = separarEmprestimosAlteracao(emprestimos,
				configuracoes);
		List<ConfiguracaoEmprestimoConsignataria> configuracoesExcluir = separarEmprestimosExclusao(emprestimos,
				configuracoes);
		this.configuracaoEmprestimoConsignatariaRepository.deleteAll(configuracoesExcluir);
		this.configuracaoEmprestimoConsignatariaRepository.saveAll(configuracoesAlterar);
		this.configuracaoEmprestimoConsignatariaRepository.saveAll(configuracoesIncluir);
	}

	private List<ConfiguracaoEmprestimoConsignataria> separarEmprestimosExclusao(
			List<ConfiguracaoEmprestimoConsignataria> emprestimos,
			List<ConfiguracaoEmprestimoConsignataria> configuracoes) {
		List<ConfiguracaoEmprestimoConsignataria> configuracoesExclusao = new ArrayList<>();
		for (ConfiguracaoEmprestimoConsignataria c : configuracoes) {
			boolean achou = false;
			for (ConfiguracaoEmprestimoConsignataria e : emprestimos) {
				if (c.getPk().equals(e.getPk())) {
					achou = true;
					break;
				}
			}
			if (!achou) {
				configuracoesExclusao.add(c);
			}
		}
		return configuracoesExclusao;
	}

	private List<ConfiguracaoEmprestimoConsignataria> separarEmprestimosAlteracao(
			List<ConfiguracaoEmprestimoConsignataria> emprestimos,
			List<ConfiguracaoEmprestimoConsignataria> configuracoes) {
		List<ConfiguracaoEmprestimoConsignataria> configuracoesAlteracao = new ArrayList<>();
		for (ConfiguracaoEmprestimoConsignataria c : configuracoes) {
			for (ConfiguracaoEmprestimoConsignataria e : emprestimos) {
				if (!e.isNew() && c.getPk().equals(e.getPk())) {
					BeanUtils.copyProperties(e, c, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
							"usuarioAlteracao", "dataHoraAlteracao", "produto");
					configuracoesAlteracao.add(c);
				}
			}
		}
		return configuracoesAlteracao;
	}

	private List<ConfiguracaoEmprestimoConsignataria> separarEmprestimosInclusao(
			ProdutoConsignataria produtoConsignataria, List<ConfiguracaoEmprestimoConsignataria> emprestimos,
			List<ConfiguracaoEmprestimoConsignataria> configuracoes) {
		List<ConfiguracaoEmprestimoConsignataria> configuracoesInclusao = new ArrayList<>();
		for (ConfiguracaoEmprestimoConsignataria c : emprestimos) {
			if (c.isNew()) {
				c.setProduto(produtoConsignataria);
				configuracoesInclusao.add(c);
			}
		}
		return configuracoesInclusao;
	}

	public List<ConfiguracaoRenegociacaoEmprestimoConsignataria> insertRenegociacao(
			@Valid List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos) {
		ProdutoConsignataria produto = createproduto();
		produto.setProduto(ProdutoConsignado.RENEGOCIACAO_EMPRESTIMO);
		produtoConsignatariaRepository.save(produto);
		for (ConfiguracaoRenegociacaoEmprestimoConsignataria emprestimo : emprestimos) {
			emprestimo.setProduto(produto);
		}
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> salvos = this.configuracaoRenegociacaoEmprestimoConsignatariaRepository
				.saveAll(emprestimos);
		return salvos;

	}

	public List<ConfiguracaoRenegociacaoEmprestimoConsignataria> parserArquivoParcelasRenegociacao(MultipartFile file)
			throws SpreadsheetReadException, IOException {
		final XlsxReader reader = new XlsxReader();
		return reader.read(ConfiguracaoRenegociacaoEmprestimoConsignataria.class, file.getInputStream());
	}

	public List<ConfiguracaoRenegociacaoEmprestimoConsignataria> findRenegociacao(Long pk) {
		return this.configuracaoRenegociacaoEmprestimoConsignatariaRepository.findByProduto_pk(pk);
	}

	public void atualizarRenegociacoes(Long pk,
			@Valid List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos) {
		ProdutoConsignataria produtoConsignataria = produtoConsignatariaRepository.findById(pk).get();
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoes = configuracaoRenegociacaoEmprestimoConsignatariaRepository
				.findByProduto_pk(pk);
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoesIncluir = separarRenegociacaoInclusao(
				produtoConsignataria, emprestimos, configuracoes);
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoesAlterar = separarRenegociacaoAlteracao(
				emprestimos, configuracoes);
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoesExcluir = separarRenegociacaoExclusao(
				emprestimos, configuracoes);
		this.configuracaoRenegociacaoEmprestimoConsignatariaRepository.deleteAll(configuracoesExcluir);
		this.configuracaoRenegociacaoEmprestimoConsignatariaRepository.saveAll(configuracoesAlterar);
		this.configuracaoRenegociacaoEmprestimoConsignatariaRepository.saveAll(configuracoesIncluir);
	}

	private List<ConfiguracaoRenegociacaoEmprestimoConsignataria> separarRenegociacaoExclusao(
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos,
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoes) {
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoesExclusao = new ArrayList<>();
		for (ConfiguracaoRenegociacaoEmprestimoConsignataria c : configuracoes) {
			boolean achou = false;
			for (ConfiguracaoRenegociacaoEmprestimoConsignataria e : emprestimos) {
				if (c.getPk().equals(e.getPk())) {
					achou = true;
					break;
				}
			}
			if (!achou) {
				configuracoesExclusao.add(c);
			}
		}
		return configuracoesExclusao;
	}

	private List<ConfiguracaoRenegociacaoEmprestimoConsignataria> separarRenegociacaoAlteracao(
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos,
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoes) {
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoesAlteracao = new ArrayList<>();
		for (ConfiguracaoRenegociacaoEmprestimoConsignataria c : configuracoes) {
			for (ConfiguracaoRenegociacaoEmprestimoConsignataria e : emprestimos) {
				if (!e.isNew() && c.getPk().equals(e.getPk())) {
					BeanUtils.copyProperties(e, c, "pk", "versao", "usuarioInclusao", "dataHoraInclusao",
							"usuarioAlteracao", "dataHoraAlteracao", "produto");
					configuracoesAlteracao.add(c);
				}
			}
		}
		return configuracoesAlteracao;
	}

	private List<ConfiguracaoRenegociacaoEmprestimoConsignataria> separarRenegociacaoInclusao(
			ProdutoConsignataria produtoConsignataria,
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos,
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoes) {
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configuracoesInclusao = new ArrayList<>();
		for (ConfiguracaoRenegociacaoEmprestimoConsignataria c : emprestimos) {
			if (c.isNew()) {
				c.setProduto(produtoConsignataria);
				configuracoesInclusao.add(c);
			}
		}
		return configuracoesInclusao;
	}

}

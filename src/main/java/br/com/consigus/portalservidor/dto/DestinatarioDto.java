package br.com.consigus.portalservidor.dto;

import br.com.consigus.portalservidor.enums.TipoUsuario;
import lombok.Data;

@Data
public class DestinatarioDto {
	
	private Long idDestinatario;
	
	private TipoUsuario destinatario;
	
	private String nomeDestinatario;
	

}

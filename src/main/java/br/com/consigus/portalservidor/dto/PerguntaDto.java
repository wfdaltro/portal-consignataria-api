package br.com.consigus.portalservidor.dto;

import javax.persistence.Column;

import lombok.Data;

@Data
public class PerguntaDto {
	
	private String titulo;

	private String resposta;

}

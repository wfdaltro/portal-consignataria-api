package br.com.consigus.portalservidor.dto;

import java.util.List;

import lombok.Data;

@Data
public class FaqsDto {
	
	private String categoria;
	
	private List<PerguntaDto> perguntas;

}

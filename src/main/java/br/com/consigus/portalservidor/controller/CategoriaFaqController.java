package br.com.consigus.portalservidor.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.CategoriaFaq;
import br.com.consigus.portalservidor.entity.Servidor;

@RestController
@RequestMapping("/categoriasFaq")
public class CategoriaFaqController  extends ConsignusController<CategoriaFaq, Long> {

}

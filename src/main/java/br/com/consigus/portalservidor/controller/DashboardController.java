package br.com.consigus.portalservidor.controller;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.Aviso;
import br.com.consigus.portalservidor.enums.DestinatarioAviso;
import br.com.consigus.portalservidor.service.AvisoService;

@RestController
@RequestMapping("/dashboard")
public class DashboardController {
	
	@Autowired
	private AvisoService avisoService;
	
	@GetMapping(value = "/avisos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Aviso>> search(@RequestParam Map<String, String> params) {
		Collection<Aviso> registros = this.avisoService.findByDestinatarioAviso();
		return ResponseEntity.ok().body(registros);
	}
	

}

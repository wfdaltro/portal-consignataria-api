package br.com.consigus.portalservidor.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.ParcelasEmprestimoConsignado;
import br.com.consigus.portalservidor.service.EmprestimoConsignadoService;

@RestController
@RequestMapping("/emprestimosConsignados")
public class EmprestimoConsignadoController {

	@Autowired
	private EmprestimoConsignadoService emprestimoConsignadoService;

	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<EmprestimoConsignado>> search(@RequestParam Map<String, String> params) {
		Collection<EmprestimoConsignado> registros = this.emprestimoConsignadoService.findAll(params);
		return ResponseEntity.ok().body(registros);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmprestimoConsignado> findOne(@PathVariable("id") Long id) {
		EmprestimoConsignado objetoEntidade = this.emprestimoConsignadoService.findOne(id);
		return ResponseEntity.ok().body(objetoEntidade);
	}

	@GetMapping(value = "/parcelas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<ParcelasEmprestimoConsignado>> parcelas(@RequestParam("id") Long id) {
		Collection<ParcelasEmprestimoConsignado> objetos = this.emprestimoConsignadoService.parcelas(id);
		return ResponseEntity.ok().body(objetos);
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "/ocorrencia/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Void> ocorrencia(@PathVariable("id") Long id, @Valid @RequestBody DadosOperacao dadosOperacao) {
		this.emprestimoConsignadoService.ocorrencia(id, dadosOperacao);
		return ResponseEntity.ok().build();
	}

	@GetMapping(value = "/anexos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<DocumentoEmprestimoConsignado>> anexos(@RequestParam("id") Long id) {
		Collection<DocumentoEmprestimoConsignado> objetos = this.emprestimoConsignadoService.anexos(id);
		return ResponseEntity.ok().body(objetos);
	}

	@PostMapping(value = "/anexo/add")
	public ResponseEntity<?> inserirAnexo(@RequestParam("idEmprestimo") Long idEmprestimo,@RequestParam("nome") String nome,
			@RequestParam("file") MultipartFile file) throws IOException {
		emprestimoConsignadoService.inserirAnexos(idEmprestimo, file, nome);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@DeleteMapping(value = "/anexo/delete")
	public ResponseEntity<?> excluirAnexo(@RequestParam("idDocumento") Integer idDocumento) throws IOException {
		emprestimoConsignadoService.removerArquivo(idDocumento);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@GetMapping(path = "/anexo/download")
	public ResponseEntity<?> download(@RequestParam("idAnexo") Integer idAnexo) throws IOException {
		ArquivoDocumentoEmprestimoConsignado arquivo = emprestimoConsignadoService.obterAnexo(idAnexo);
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentLength(arquivo.getDocumento().getTamanho());
		respHeaders.setContentType(MediaType.parseMediaType(arquivo.getContentType()));
		respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + arquivo.getDocumento().getNome());
		return new ResponseEntity<byte[]>(arquivo.getArquivo(), respHeaders, HttpStatus.OK);
	}
	
	@GetMapping(value = "/servidor/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<EmprestimoConsignado>> findByServidor(@PathVariable("id") Long id) {
		Collection<EmprestimoConsignado> registros = this.emprestimoConsignadoService.findByServidor(id);
		return ResponseEntity.ok().body(registros);
	}

}

package br.com.consigus.portalservidor.controller;

import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.Servidor;
import br.com.consigus.portalservidor.service.ServidorService;

@RestController
@RequestMapping("/servidores")
public class ServidorController {

	@Autowired
	private ServidorService service;

	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Servidor>> search(@RequestParam Map<String, String> params) {
		Collection<Servidor> registros = this.service.findAll(params);
		return ResponseEntity.ok().body(registros);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Servidor> findOne(@PathVariable("id") Long id) {
		Servidor objetoEntidade = this.service.findOne(id);
		return ResponseEntity.ok().body(objetoEntidade);
	}

	@GetMapping(value = "/cpf", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Servidor> findByCpf(@RequestParam("idServidor") String cpf) {
		Servidor objetoEntidade = this.service.findByCpf(cpf);
		return ResponseEntity.ok().body(objetoEntidade);
	}
	
	@GetMapping(value = "/cpfMatricula", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Servidor> findCpfMatricula(@RequestParam("k") String chave) {
		Servidor objetoEntidade = this.service.findByCpfMatricula(chave);
		return ResponseEntity.ok().body(objetoEntidade);
	}

}

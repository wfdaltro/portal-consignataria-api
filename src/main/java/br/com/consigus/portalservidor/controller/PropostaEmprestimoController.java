package br.com.consigus.portalservidor.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.consigus.portalservidor.dto.DadosOperacao;
import br.com.consigus.portalservidor.dto.OfertaEmprestimo;
import br.com.consigus.portalservidor.entity.ArquivoDocumentoPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.OcorrrenciaPropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.entity.PropostaEmprestimoConsignado;
import br.com.consigus.portalservidor.service.PropostaEmprestimoConsignadoService;
import br.com.consigus.portalservidor.service.SimuladorEmprestimoService;

@RestController
@RequestMapping("/propostaEmprestimo")
public class PropostaEmprestimoController {

	@Autowired
	private SimuladorEmprestimoService simuladorEmprestimoService;
	
	@Autowired
	private PropostaEmprestimoConsignadoService service;

	@GetMapping(value = "simular/valorParcela", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OfertaEmprestimo> simularPorParcelas(@RequestParam("idServidor") Long idServidor,
			@RequestParam("valor") Double valor, @RequestParam("parcelas") Integer parcelas) throws ParseException {
		OfertaEmprestimo ofertas = simuladorEmprestimoService.simularPorValorParcelas(idServidor, valor, parcelas);
		return ResponseEntity.ok().body(ofertas);
	}

	@GetMapping(value = "simular/valorFinanciamento", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OfertaEmprestimo> simularPorValor(@RequestParam("idServidor") Long idServidor,
			@RequestParam("valor") Double valor, @RequestParam("parcelas") Integer parcelas) throws ParseException {
		OfertaEmprestimo ofertas = simuladorEmprestimoService.simularPorValorTotal(idServidor, valor, parcelas);
		return ResponseEntity.ok().body(ofertas);
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<PropostaEmprestimoConsignado> insert(@Valid @RequestBody PropostaEmprestimoConsignado entidade) throws AddressException, MessagingException {
		PropostaEmprestimoConsignado entiddeSalva = this.service.insert(entidade);
		return ResponseEntity.ok().body(entiddeSalva);
	}
	
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "averbar/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<EmprestimoConsignado> patch(@PathVariable("id") Long id, @Valid @RequestBody String codigoAverbacao) {
		EmprestimoConsignado objetoEntidade = this.service.averbar(id, codigoAverbacao);
		return ResponseEntity.ok().body(objetoEntidade);
	}
	
	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<PropostaEmprestimoConsignado>> search(@RequestParam Map<String, String> params) {
		Collection<PropostaEmprestimoConsignado> registros = this.service.findAll(params);
		return ResponseEntity.ok().body(registros);
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "/ocorrencia/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Void> ocorrencia(@PathVariable("id") Long id, @Valid @RequestBody DadosOperacao dadosOperacao) {
		this.service.ocorrencia(id, dadosOperacao);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<PropostaEmprestimoConsignado> findOne(@PathVariable("id") Long id) {
		PropostaEmprestimoConsignado objetoEntidade = this.service.findOne(id);
		return ResponseEntity.ok().body(objetoEntidade);
	}
	

	@GetMapping(value = "/anexos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<DocumentoPropostaEmprestimoConsignado>> anexos(@RequestParam("id") Long id) {
		Collection<DocumentoPropostaEmprestimoConsignado> objetos = this.service.anexos(id);
		return ResponseEntity.ok().body(objetos);
	}

	@PostMapping(value = "/anexo/add")
	public ResponseEntity<?> inserirAnexo(@RequestParam("idEmprestimo") Long idEmprestimo,@RequestParam("nome") String nome,
			@RequestParam("file") MultipartFile file) throws IOException {
		service.inserirAnexos(idEmprestimo, file, nome);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@DeleteMapping(value = "/anexo/delete")
	public ResponseEntity<?> excluirAnexo(@RequestParam("idDocumento") Integer idDocumento) throws IOException {
		service.removerArquivo(idDocumento);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@GetMapping(path = "/anexo/download")
	public ResponseEntity<?> download(@RequestParam("idAnexo") Integer idAnexo) throws IOException {
		ArquivoDocumentoPropostaEmprestimoConsignado arquivo = service.obterAnexo(idAnexo);
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentLength(arquivo.getDocumento().getTamanho());
		respHeaders.setContentType(MediaType.parseMediaType(arquivo.getContentType()));
		respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + arquivo.getDocumento().getNome());
		return new ResponseEntity<byte[]>(arquivo.getArquivo(), respHeaders, HttpStatus.OK);
	}
	
	@GetMapping(value = "/ocorrencias/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<List<OcorrrenciaPropostaEmprestimoConsignado> > findOcorrencias(@PathVariable("id") Long id) {
		List<OcorrrenciaPropostaEmprestimoConsignado> ocorrencias = this.service.findOcorrencias(id);
		return ResponseEntity.ok().body(ocorrencias);
	}
	
	
	@GetMapping(path = "/termo/download")
	public void downloadTermo(@RequestParam("idProposta") Long idProposta,  HttpServletResponse httpServletResponse) throws IOException {
		byte[] rel =  this.service.gerarRelatorioAutorizacaoDesconto(idProposta);
		
		
		 ByteArrayOutputStream out = new ByteArrayOutputStream(rel.length);
		    out.write(rel, 0, rel.length);

		    httpServletResponse.setContentType("application/pdf");
		    httpServletResponse.addHeader("Content-Disposition", "inline; filename=autorização_de_desconto.pdf");

		    OutputStream os;
		    try {
		        os = httpServletResponse.getOutputStream();
		        out.writeTo(os);
		        os.flush();
		        os.close();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }

	}
	
	
	

}
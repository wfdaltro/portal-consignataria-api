package br.com.consigus.portalservidor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.exception.NegocioException;
import br.com.consigus.portalservidor.security.UsuarioAutenticado;
import br.com.consigus.portalservidor.service.OperadorConsignatariaService;

@RestController
public class UsuarioAutenticadoController {

	@Autowired
	private OperadorConsignatariaService service;

	@GetMapping(value = "sessionInfo", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UsuarioAutenticado> sessionInfo() throws NegocioException {
		UsuarioAutenticado autenticado = this.service.obterDadosUsuarioAutenticado();
		return ResponseEntity.ok().body(autenticado);
	}

}
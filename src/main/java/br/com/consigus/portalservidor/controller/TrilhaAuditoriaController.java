package br.com.consigus.portalservidor.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.TrilhaAuditoria;
import br.com.consigus.portalservidor.service.TrilhaAuditoriaService;

@RestController
@RequestMapping("/trilhaAuditoria")
public class TrilhaAuditoriaController {

	@Autowired
	private TrilhaAuditoriaService trilhaAuditoriaService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TrilhaAuditoria>> listar() {
		List<TrilhaAuditoria> trilhasAuditoria = trilhaAuditoriaService.listar();
		return ResponseEntity.ok().body(trilhasAuditoria);
	}

}

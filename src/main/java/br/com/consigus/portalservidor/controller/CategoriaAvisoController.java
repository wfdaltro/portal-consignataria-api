package br.com.consigus.portalservidor.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.CategoriaAviso;
import br.com.consigus.portalservidor.entity.Servidor;

@RestController
@RequestMapping("/categoriasAviso")
public class CategoriaAvisoController  extends ConsignusController<CategoriaAviso, Long> {
}

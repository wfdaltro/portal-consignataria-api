package br.com.consigus.portalservidor.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.Aviso;

@RestController
@RequestMapping("/avisos")
public class AvisoController  extends ConsignusController<Aviso, Long> {

}

package br.com.consigus.portalservidor.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.Parametros;

@RestController
@RequestMapping("/parametros")
public class ParametrosController extends ConsignusController<Parametros, Integer> {

}

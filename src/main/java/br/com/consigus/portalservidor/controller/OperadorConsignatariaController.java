package br.com.consigus.portalservidor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.service.OperadorConsignatariaService;

@RestController
@RequestMapping("/operadoresConsignataria")
public class OperadorConsignatariaController extends ConsignusController<OperadorEntidadeConsignataria, Long> {

	@Autowired
	private OperadorConsignatariaService service;

}

package br.com.consigus.portalservidor.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.EmprestimoConsignado;
import br.com.consigus.portalservidor.entity.Portabilidade;
import br.com.consigus.portalservidor.service.PortabilidadeService;

@RestController
@RequestMapping("/portabilidade")
public class PortabilidadeController extends ConsignusController<Portabilidade, Long> {

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "finalizar/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<EmprestimoConsignado> patch(@PathVariable("id") Long id) {
		((PortabilidadeService) this.service).finalizar(id);
		return ResponseEntity.ok().body(null);
	}

}

package br.com.consigus.portalservidor.controller;

import java.text.ParseException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.dto.OfertaEmprestimo;
import br.com.consigus.portalservidor.entity.PropostaRenegociacaoConsignado;
import br.com.consigus.portalservidor.service.RenegociacaoService;
import br.com.consigus.portalservidor.service.SimuladorRenegociacaoService;

@RestController
@RequestMapping("/renegociacaoEmprestimo")
public class RenegociacaoEmprestimoController {

	@Autowired
	private SimuladorRenegociacaoService simuladorRenegociacaoService;
	
	@Autowired
	private RenegociacaoService renegociacaoService;
	

	@GetMapping(value = "simular/valorParcela", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OfertaEmprestimo> simularRenegociacao(@RequestParam("idServidor") Long idServidor,
			@RequestParam("valor") Double valor, @RequestParam("parcelas") Integer parcelas) throws ParseException {
		OfertaEmprestimo ofertas = simuladorRenegociacaoService.simularPorValorParcelas(idServidor, valor, parcelas);
		return ResponseEntity.ok().body(ofertas);
	}
	
	@GetMapping(value = "simular/valorFinanciamento", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<OfertaEmprestimo> simularPorValor(@RequestParam("idServidor") Long idServidor,
			@RequestParam("valor") Double valor, @RequestParam("parcelas") Integer parcelas) throws ParseException {
		OfertaEmprestimo ofertas = simuladorRenegociacaoService.simularPorValorTotal(idServidor, valor, parcelas);
		return ResponseEntity.ok().body(ofertas);
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<PropostaRenegociacaoConsignado> insert(@Valid @RequestBody PropostaRenegociacaoConsignado entidade) throws AddressException, MessagingException {
		PropostaRenegociacaoConsignado entiddeSalva = this.renegociacaoService.insert(entidade);
		return ResponseEntity.ok().body(entiddeSalva);
	}
	

}

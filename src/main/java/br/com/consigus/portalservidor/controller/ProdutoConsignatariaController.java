
package br.com.consigus.portalservidor.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.consigus.portalservidor.entity.ConfiguracaoCartaoConsignataria;
import br.com.consigus.portalservidor.entity.ConfiguracaoEmprestimoConsignataria;
import br.com.consigus.portalservidor.entity.ConfiguracaoRenegociacaoEmprestimoConsignataria;
import br.com.consigus.portalservidor.entity.ProdutoConsignataria;
import br.com.consigus.portalservidor.service.ProdutoConsignatariaService;
import io.github.millij.poi.SpreadsheetReadException;

@RestController
@RequestMapping("/produtoConsignataria")
@CrossOrigin(origins = "*")
public class ProdutoConsignatariaController {

	@Autowired
	private ProdutoConsignatariaService produtoConsignatariaService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<ProdutoConsignataria>> search(@RequestParam Map<String, String> params) {
		Collection<ProdutoConsignataria> registros = this.produtoConsignatariaService.findAll();		
		return ResponseEntity.ok().body(registros);
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PostMapping(value = "/cartao", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConfiguracaoCartaoConsignataria> insertCartao(
			@Valid @RequestBody ConfiguracaoCartaoConsignataria cartao) {
		ConfiguracaoCartaoConsignataria c = this.produtoConsignatariaService.insertCartao(cartao);
		return ResponseEntity.ok().body(c);
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PostMapping(value = "/emprestimo", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> insertEmprestimo(
			@Valid @RequestBody List<ConfiguracaoEmprestimoConsignataria> emprestimos) {
		this.produtoConsignatariaService.insertEmprestimo(emprestimos);
		return ResponseEntity.ok().build();
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PostMapping(value = "/renegociacao", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> insertRenegociacao(
			@Valid @RequestBody List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos) {
		this.produtoConsignatariaService.insertRenegociacao(emprestimos);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/emprestimo/uploadArquivoParcelas")
	public ResponseEntity<List<ConfiguracaoEmprestimoConsignataria>> uploadArquivoParcelas(
			@RequestParam("file") MultipartFile file) {
		try {
			List<ConfiguracaoEmprestimoConsignataria> configs = this.produtoConsignatariaService
					.parserArquivoParcelas(file);
			return ResponseEntity.ok().body(configs);
		} catch (SpreadsheetReadException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@PostMapping("/renegociacao/uploadArquivoParcelas")
	public ResponseEntity<List<ConfiguracaoRenegociacaoEmprestimoConsignataria>> uploadArquivoParcelasRenegociacao(
			@RequestParam("file") MultipartFile file) {
		try {
			List<ConfiguracaoRenegociacaoEmprestimoConsignataria> configs = this.produtoConsignatariaService
					.parserArquivoParcelasRenegociacao(file);
			return ResponseEntity.ok().body(configs);
		} catch (SpreadsheetReadException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping(value = "/cartao/{pk}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ConfiguracaoCartaoConsignataria> findCartao(@PathVariable("pk") Long pk) {
		ConfiguracaoCartaoConsignataria cartao = this.produtoConsignatariaService.findCartao(pk);
		return ResponseEntity.ok().body(cartao);
	}

	@GetMapping(value = "/emprestimo/{pk}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ConfiguracaoEmprestimoConsignataria>> findEmprestimo(@PathVariable("pk") Long pk) {
		List<ConfiguracaoEmprestimoConsignataria> emprestimos = this.produtoConsignatariaService.findEmprestimo(pk);
		return ResponseEntity.ok().body(emprestimos);
	}
	
	@GetMapping(value = "/renegociacao/{pk}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ConfiguracaoRenegociacaoEmprestimoConsignataria>> findRenegociacao(@PathVariable("pk") Long pk) {
		List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos = this.produtoConsignatariaService.findRenegociacao(pk);
		return ResponseEntity.ok().body(emprestimos);
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "cartao", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> atualizarCartao(@Valid @RequestBody ConfiguracaoCartaoConsignataria cartao) {
		this.produtoConsignatariaService.atualizarCartao(cartao);
		return ResponseEntity.ok().build();
	}

	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "/emprestimo/{pk}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> atualizarEmprestimos(@PathVariable("pk") Long pk,
			@Valid @RequestBody List<ConfiguracaoEmprestimoConsignataria> emprestimos) {
		this.produtoConsignatariaService.atualizarEmprestimos(pk, emprestimos);
		return ResponseEntity.ok().build();
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	@PatchMapping(value = "/renegociacao/{pk}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> atualizarRenegociacoes(@PathVariable("pk") Long pk,
			@Valid @RequestBody List<ConfiguracaoRenegociacaoEmprestimoConsignataria> emprestimos) {
		this.produtoConsignatariaService.atualizarRenegociacoes(pk, emprestimos);
		return ResponseEntity.ok().build();
	}

}

package br.com.consigus.portalservidor.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.consigus.portalservidor.entity.ArquivoDocumentoCartaoConsignado;
import br.com.consigus.portalservidor.entity.CartaoConsignado;
import br.com.consigus.portalservidor.entity.DocumentoCartaoConsignado;
import br.com.consigus.portalservidor.service.CartaoConsignadoService;

@RestController
@RequestMapping("/cartoesConsignados")
public class CartaoCreditoConsignadoController {
	
	@Autowired
	private CartaoConsignadoService cartaoConsignadoService;

	@GetMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<CartaoConsignado>> search(@RequestParam Map<String, String> params) {
		Collection<CartaoConsignado> registros = this.cartaoConsignadoService.findAll(params);
		return ResponseEntity.ok().body(registros);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CartaoConsignado> findOne(@PathVariable("id") Long id) {
		CartaoConsignado objetoEntidade = this.cartaoConsignadoService.findOne(id);
		return ResponseEntity.ok().body(objetoEntidade);
	}

 
	@GetMapping(value = "/anexos", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<DocumentoCartaoConsignado>> anexos(@RequestParam("id") Long id) {
		Collection<DocumentoCartaoConsignado> objetos = this.cartaoConsignadoService.anexos(id);
		return ResponseEntity.ok().body(objetos);
	}

	@PostMapping(value = "/anexo/add")
	public ResponseEntity<?> inserirAnexo(@RequestParam("idEmprestimo") Long idEmprestimo,@RequestParam("nome") String nome,
			@RequestParam("file") MultipartFile file) throws IOException {
		cartaoConsignadoService.inserirAnexos(idEmprestimo, file, nome);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@DeleteMapping(value = "/anexo/delete")
	public ResponseEntity<?> excluirAnexo(@RequestParam("idDocumento") Integer idDocumento) throws IOException {
		cartaoConsignadoService.removerArquivo(idDocumento);
		return ResponseEntity.status(HttpStatus.OK).build();

	}

	@GetMapping(path = "/anexo/download")
	public ResponseEntity<?> download(@RequestParam("idAnexo") Integer idAnexo) throws IOException {
		ArquivoDocumentoCartaoConsignado arquivo = cartaoConsignadoService.obterAnexo(idAnexo);
		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentLength(arquivo.getDocumento().getTamanho());
		respHeaders.setContentType(MediaType.parseMediaType(arquivo.getContentType()));
		respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + arquivo.getDocumento().getNome());
		return new ResponseEntity<byte[]>(arquivo.getArquivo(), respHeaders, HttpStatus.OK);
	}


}

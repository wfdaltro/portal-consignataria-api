package br.com.consigus.portalservidor.controller;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/enums")
public class EnumController {

	private final String ENUM_PACKAGE = "br.com.consigus.portalservidor.enums";
										 

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Object>> search(@RequestParam("nome") String enumName) {
		try {
			Class clazz = Class.forName(ENUM_PACKAGE + "." + enumName);
			return ResponseEntity.ok().body(Arrays.asList(clazz.getEnumConstants()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}

	}

}

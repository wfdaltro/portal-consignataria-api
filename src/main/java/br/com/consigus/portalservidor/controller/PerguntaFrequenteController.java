package br.com.consigus.portalservidor.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.dto.FaqsDto;
import br.com.consigus.portalservidor.service.PerguntaFrequenteService;


@RestController
@RequestMapping("/perguntasFrequentes")
public class PerguntaFrequenteController{
	
	@Autowired
	private PerguntaFrequenteService  service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<FaqsDto>> findAll() {
		List<FaqsDto> registros = this.service.findAll();
		return ResponseEntity.ok().body(registros);
	}


}

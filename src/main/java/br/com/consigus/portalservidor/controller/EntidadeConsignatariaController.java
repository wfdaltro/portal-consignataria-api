package br.com.consigus.portalservidor.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.consigus.portalservidor.entity.EntidadeConsignataria;
import br.com.consigus.portalservidor.service.EntidadeConsignatariaService;

@RestController
@RequestMapping("/consignatarias")
public class EntidadeConsignatariaController extends ConsignusController<EntidadeConsignataria, Long> {

	@GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<EntidadeConsignataria> find() {
		EntidadeConsignataria objetoEntidade = this.getService().get();
		return ResponseEntity.ok().body(objetoEntidade);
	}

	private EntidadeConsignatariaService getService() {
		return (EntidadeConsignatariaService) this.service;
	}

}

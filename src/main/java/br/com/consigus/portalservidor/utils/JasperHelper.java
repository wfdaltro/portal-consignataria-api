package br.com.consigus.portalservidor.utils;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Component
public class JasperHelper {

	private static final String EXTENSAO_RELATORIOS = ".jrxml";
	private static final String DIR_RELATORIOS = "relatorios/";

	public byte[] pdf(String nomeRelatorio, Map<String, Object> parameters, List<?> rows) {
		try {
			ClassPathResource reportResource = new ClassPathResource(DIR_RELATORIOS + nomeRelatorio+ EXTENSAO_RELATORIOS);
			final InputStream stream = reportResource.getInputStream();
			final JasperReport report = JasperCompileManager.compileReport(stream);
			JRDataSource source = new JRBeanCollectionDataSource(rows);
			if (rows == null || rows.size() == 0) {
				source = new JREmptyDataSource(1);
			} 
			final JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, source);
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

}

package br.com.consigus.portalservidor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalConsignatariaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalConsignatariaApplication.class, args);
	}

}

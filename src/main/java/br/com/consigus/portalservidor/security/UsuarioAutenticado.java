package br.com.consigus.portalservidor.security;

import java.io.Serializable;

import br.com.consigus.portalservidor.enums.SituacaoUsuario;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UsuarioAutenticado implements Serializable {

	private Long pk;

	private String login;

	private String matricula;

	private String nome;
	
	private SituacaoUsuario situacao;
	
	private boolean senhaExpirada;
	
	private String senha;
	
	private boolean master;

}

package br.com.consigus.portalservidor.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.consigus.portalservidor.entity.OperadorEntidadeConsignataria;
import br.com.consigus.portalservidor.repository.OperadorEntidadeConsignatariaRepository;

@Repository
public class JwtAuthenticationRepository {

	@Autowired
	private OperadorEntidadeConsignatariaRepository operadorEntidadeConsignatariaRepository;

	@Transactional(readOnly = true)
	public UsuarioAutenticado loadUserByUsername(String username) {
		try {
			OperadorEntidadeConsignataria usuario = operadorEntidadeConsignatariaRepository.findByLogin(username);
			return new UsuarioAutenticado(usuario.getPk(), usuario.getLogin(), usuario.getMatricula(), usuario.getNome(),
					usuario.getSituacao(), false, usuario.getSenha(), usuario.isMaster());
		} catch (EmptyResultDataAccessException e) {
			throw new UsernameNotFoundException(username);
		}
	}

}

package br.com.consigus.portalservidor.mail;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.consigus.portalservidor.entity.Parametros;
import br.com.consigus.portalservidor.repository.ParametrosRepository;

@Component
public class EmailHelper {

	@Autowired
	private ParametrosRepository parametrosRepository;

	public void enviarEmail(String destinatario, String assunto, String msg)
			throws AddressException, MessagingException {
		Parametros parametros = parametrosRepository.findAll().get(0);
		Message message = new MimeMessage(criarSessionMail(parametros));
		message.setFrom(new InternetAddress(parametros.getRemetenteEmailSistema()));
		Address[] toUser = InternetAddress.parse(destinatario.trim().toLowerCase());
		message.setRecipients(Message.RecipientType.TO, toUser);
		message.setSubject(assunto);
		message.setContent(msg, "text/html");
		Transport.send(message);
	}

	private Session criarSessionMail(Parametros parametros) {
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", parametros.getProtocoloEnvioEmail());
		props.put("mail.smtp.socketFactory.port", parametros.getPortaEnvioEmail());
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", parametros.getPortaEnvioEmail());
		props.put("mail.smtp.host", parametros.getServidorEnvioEmail());

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(parametros.getUsuarioEnvioEmail().trim(), parametros.getSenhaEnvioEmail().trim());
			}
		});
		session.setDebug(true);
		return session;
	}
	
	
	public static void main(String... args) {
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.socketFactory.port", "587");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.host", "smtp.gmail.com");

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("sistema.consignus", "consignado1");
			}
		});
		session.setDebug(true);
		Message message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress("eu@eutesto.com"));
			Address[] toUser = InternetAddress.parse("wfdaltro@gmail.com");
			message.setRecipients(Message.RecipientType.TO, toUser);
			message.setSubject("Consignus mandando mendsagem do gmail");
			message.setContent("Consignus mandando mendsagem do gmail", "text/html");
			Transport.send(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	


}
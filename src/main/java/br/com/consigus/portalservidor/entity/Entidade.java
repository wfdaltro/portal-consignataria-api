package br.com.consigus.portalservidor.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@Data
@EqualsAndHashCode
public abstract class Entidade<K> implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected K pk;

	@JsonIgnore
	public boolean isNew() {
		return this.pk == null;
	}

}

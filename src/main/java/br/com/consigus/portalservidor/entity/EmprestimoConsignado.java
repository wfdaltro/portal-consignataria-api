package br.com.consigus.portalservidor.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_EMPRESTIMO_CONSIGNADO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_EMPRESTIMO_CONSIGNADO"))
public class EmprestimoConsignado extends EntidadeAuditavel<Long> {
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SERVIDOR", unique = false, nullable = true, updatable = true)
	private Servidor servidor;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ENTIDADE_CONSIGNATARIA", unique = false, nullable = true, updatable = true)
	private EntidadeConsignataria consignataria;
	
	@Column(name = "CD_SITUACAO_CONSIGNACAO")
	@Convert(converter = SituacaoConsignacao.Converter.class)
	private SituacaoConsignacao situacao;
	
	@Column(name = "NU_ADE")
	private String ade;
	
	@Column(name = "VL_LIBERADO")
	private BigDecimal valorTotalFinanciamento;

	@Column(name = "VL_FINANCIADO")
	private BigDecimal valorFinanciado;

	@Column(name = "VL_PRESTACAO")
	private BigDecimal valorPrestacao;

	@Column(name = "VL_SALDO_RESIDUAL")
	private BigDecimal totalSaldoResidual;

	@Column(name = "NU_PARCELAS")
	private Integer totalDeParcelas;

	@Column(name = "NU_PARCELAS_ABERTO")
	private Integer parcelasEmAberto;

	@Column(name = "VL_JUROS_MENSAL")
	private Double jurosAoMes;

	@Column(name = "VL_TAC")
	private Double tac;

	@Column(name = "VL_IOF")
	private Double iof;

	@Column(name = "DT_LIQUIDACAO")
	private LocalDate dataLiquidacao;

}

package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByName;

import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_CONFIGURACAO_EMPRESTIMO_CONSIGNADO_CONSIGNATARIA")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_CONSIGNADO"))
@Sheet
public class ConfiguracaoEmprestimoConsignataria extends EntidadeAuditavel<Long> {

	@Column(name = "NU_PARCELAS")
	@SheetColumn("parcelas")
	@CsvBindByName
	private Integer parcelas;

	@SheetColumn("juros")
	@CsvBindByName
	@Column(name = "VL_TAXA_JUROS")
	private double juros;

	@SheetColumn("cet")
	@CsvBindByName
	@Column(name = "VL_CET")
	private double cet;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRODUTO_CONSIGNATARIA", nullable = true)
	private ProdutoConsignataria produto;

}

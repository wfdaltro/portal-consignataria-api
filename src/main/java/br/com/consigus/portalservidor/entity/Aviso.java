package br.com.consigus.portalservidor.entity;

import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.consigus.portalservidor.enums.DestinatarioAviso;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_AVISO")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_AVISO"))
public class Aviso extends EntidadeAuditavel<Long> {

	@Column(name = "DS_TITULO")
	private String titulo;

	@Column(name = "DS_MENSAGEM")
	private String mensagem;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CATEGORIA_AVISO", unique = false, nullable = true, updatable = true)
	private CategoriaAviso categoria;

	@Column(name = "DT_EXPIRACAO" )
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dataExpiracao;

	@Column(name = "CD_DESTINATARIO", nullable = false)
	@Convert(converter = DestinatarioAviso.Converter.class)
	private DestinatarioAviso destinatarioAviso;

	@Column(name = "IN_ATIVO")
	private boolean ativo;

}

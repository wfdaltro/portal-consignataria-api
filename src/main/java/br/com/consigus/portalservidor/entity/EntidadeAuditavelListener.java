package br.com.consigus.portalservidor.entity;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;

public class EntidadeAuditavelListener {

	@PrePersist
	public void prePersist(EntidadeAuditavel<?> target) {
		LocalDateTime now = LocalDateTime.now();
		target.setDataHoraInclusao(now);
		target.setUsuarioInclusao(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
	}

	@PreUpdate
	public void preUpdate(EntidadeAuditavel<?> target) {
		LocalDateTime now = LocalDateTime.now();
		target.setDataHoraAlteracao(now);
		target.setUsuarioAlteracao(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
	}

}

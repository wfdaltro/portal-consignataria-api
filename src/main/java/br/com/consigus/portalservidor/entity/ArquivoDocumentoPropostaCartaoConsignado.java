package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_ARQUIVO_DOCUMENTO_PROPOSTA_CARTAO_CONSIGNADO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_ARQUIVO_DOCUMENTO_PROPOSTA_CARTAO_CONSIGNADO"))
public class ArquivoDocumentoPropostaCartaoConsignado extends Entidade<Integer>{
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "OB_DOCUMENTO")
	private byte[] arquivo;
	
	@Column(name = "DS_CONTENT_TYPE")
	private String contentType;
	
	@OneToOne
	@JoinColumn(name = "ID_DOCUMENTO_CARTAO_CONSIGNADO")
	private DocumentoPropostaCartaoConsignado documento;
	

}

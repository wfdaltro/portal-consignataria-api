package br.com.consigus.portalservidor.entity;

import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.consigus.portalservidor.enums.SituacaoUsuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_OPERADOR_ENTIDADE_CONSIGNATARIA")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_OPERADOR_ENTIDADE_CONSIGNATARIA"))
public class OperadorEntidadeConsignataria extends EntidadeAuditavel<Long> {

	@Column(name = "DS_NOME", nullable = false, length = 150)
	@NotNull(message = "Campo obrigatório")
	private String nome;

	@Column(name = "NU_CPF", nullable = false, unique = true, length = 30)
	@NotNull(message = "Campo obrigatório")
	private String cpf;

	@Column(name = "DS_EMAIL", nullable = false, unique = true, length = 30)
	@NotNull(message = "Campo obrigatório")
	private String email;

	@Column(name = "NU_TELEFONE")
	private String telefone;

	@Column(name = "NU_MATRICULA")
	private String matricula;

	@Column(name = "DS_ENDERECO_IP")
	private String enderecoIp;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CONSIGNATARIA", unique = false, nullable = false, updatable = false)
	private EntidadeConsignataria entidadeConsignataria;

	@Column(name = "IN_USUARIO_MASTER")
	private boolean master;
	
	@Column(name = "CD_SITUACAO", nullable = false)
	@Convert(converter = SituacaoUsuario.Converter.class)
	private SituacaoUsuario situacao;

	@Column(name = "DS_LOGIN", length = 20)
	private String login;

	@Column(name = "DS_SENHA", length = 150)
	private String senha;

	@Column(name = "DT_EXPIRACAO_SENHA")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dataExpiracaoSenha;
	

	@Transient
	private Long pkConsignataria;

}

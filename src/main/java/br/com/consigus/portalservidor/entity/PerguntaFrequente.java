package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_FAQ")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_FAQ"))
public class PerguntaFrequente extends EntidadeAuditavel<Long> {

	@Column(name = "DS_TITULO")
	private String titulo;

	@Column(name = "DS_RESPOSTA")
	private String resposta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CATEGORIA_FAQ", unique = false, nullable = true, updatable = true)
	private CategoriaFaq categoria;
	
	@Column(name = "IN_ATIVO")
	private boolean ativo;

}

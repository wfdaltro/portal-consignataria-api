package br.com.consigus.portalservidor.entity;

import java.time.LocalDateTime;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_OCORRENCIA_SERVIDOR")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_OCORRENCIA_SERVIDOR"))
public class OcorrenciaServidor extends Entidade<Integer> {

	@Column(name = "DH_OCORRENCIA")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dataHoraOcorrencia;

	@Column(name = "DS_MOTIVO")
	private String motivoOcorrencia;

	@Column(name = "DS_OCORRENCIA")
	private String descricaoOcorrencia;

	@Column(name = "DS_STATUS")
	private String statusGeradoOcorrencia;

	@Column(name = "DS_USUARIO")
	private String usuarioOcorrencia;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SERVIDOR", unique = false, nullable = true, updatable = true)
	private Servidor servidor;

}

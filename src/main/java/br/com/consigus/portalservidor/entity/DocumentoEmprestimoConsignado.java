package br.com.consigus.portalservidor.entity;

import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_DOCUMENTO_EMPRESTIMO_CONSIGNADO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_DOCUMENTO_EMPRESTIMO_CONSIGNADO"))
public class DocumentoEmprestimoConsignado extends Entidade<Integer> {
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_EMPRESTIMO_CONSIGNADO")
	private EmprestimoConsignado emprestimoConsignado;

	@Column(name = "DS_NOME")
	private String nome;
	
	@Column(name = "DS_CONTENT_TYPE")
	private String contentType;
	
	@Column(name = "DT_CADASTRO")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dataCadastro;
	
	@Column(name = "DS_USUARIO_CADASTRO")
	private String usuarioCadastro;
	
	@Column(name = "NU_TAMANHO")
	private Long tamanho;

}

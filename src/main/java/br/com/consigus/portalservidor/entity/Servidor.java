package br.com.consigus.portalservidor.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.consigus.portalservidor.enums.Estado;
import br.com.consigus.portalservidor.enums.EstadoCivil;
import br.com.consigus.portalservidor.enums.Sexo;
import br.com.consigus.portalservidor.enums.SituacaoUsuario;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_SERVIDOR")
@AttributeOverride(name = "pk", column = @Column(name = "ID_SERVIDOR"))
@Data
@EqualsAndHashCode(callSuper = true)
public class Servidor extends EntidadeAuditavel<Long> {

	@Column(name = "DS_NOME", length = 150)
	private String nome;

	@Column(name = "NU_CPF", unique = true, length = 30)
	private String cpf;

	@Column(name = "NU_RG", length = 20)
	private String rg;

	@Column(name = "DS_ORGAO_EXPEDIDOR_RG", length = 20)
	private String orgaoExpedidorRg;

	@Column(name = "DS_UF_EXPEDIDOR_RG", length = 2)
	private String ufRg;

	@Column(name = "DT_EXPEDICAO_RG")
	private LocalDate dataExpedicaoRg;

	@Column(name = "DS_NACIONALIDADE", length = 30)
	private String nacionalidade;

	@Column(name = "DS_NATURALIDADE", length = 50)
	private String naturalidade;

	@Column(name = "DS_FILIACAO_MAE", length = 100)
	private String filiacaoMae;

	@Column(name = "DS_FILIACAO_PAI", length = 100)
	private String filiacaoPai;

	@Column(name = "CD_SEXO")
	@Convert(converter = Sexo.Converter.class)
	private Sexo sexo;

	@Column(name = "CD_ESTADO_CIVIL")
	@Convert(converter = EstadoCivil.Converter.class)
	private EstadoCivil estadoCivil;

	@Column(name = "DT_NASCIMENTO")
	private LocalDate dataNascimento;

	@Column(name = "DS_EMAIL")
	private String email;

	@Column(name = "NU_TELEFONE")
	private String telefone;

	@Column(name = "NU_CELULAR")
	private String celular;

	@Column(name = "VL_RENDA_MENSAL")
	private BigDecimal rendaMensal;

	@Column(name = "NU_MATRICULA", unique = true, length = 30)
	private String matricula;

	@Column(name = "VL_MARGEM_EMPRESTIMO")
	private BigDecimal margemEmprestimo;

	@Column(name = "VL_MARGEM_CARTAO")
	private BigDecimal margemCartao;

	@Column(name = "VL_MARGEM_EMPRESTIMO_DISPONIVEL")
	private BigDecimal margemEmprestimoDisponivel;

	@Column(name = "VL_MARGEM_CARTAO_DISPONIVEL")
	private BigDecimal margemCartaoDisponivel;
	
	@Column(name = "VL_MARGEM_EMPRESTIMO_RESERVADA")
	private BigDecimal margemEmprestimoReservada;

	@Column(name = "VL_MARGEM_CARTAO_RESERVADA")
	private BigDecimal margemCartaoReservada;

	@Column(name = "DS_LOGIN", length = 20)
	private String login;

	@Column(name = "DS_SENHA", length = 150)
	private String senha;

	@Column(name = "DT_EXPIRACAO_SENHA")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dataExpiracaoSenha;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "IM_SERVIDOR")
	private byte[] foto;

	@Column(name = "DS_LOGRADOURO", length = 150)
	private String logradouro;

	@Column(name = "DS_COMPLEMENTO_LOGRADOURO", length = 100)
	private String complemento;

	@Column(name = "DS_BAIRRO", length = 100)
	private String bairro;

	@Column(name = "DS_LOCALIDADE", length = 100)
	private String localidade;

	@Column(name = "NU_CEP", length = 10)
	private String cep;

	@Column(name = "CD_ESTADO")
	@Convert(converter = Estado.Converter.class)
	private Estado estado;

	@Column(name = "DT_ADMISSAO")
	private LocalDate dataAdmissao;

	@Column(name = "DS_CARGO", length = 50)
	private String cargo;

	@Column(name = "DS_FUNCAO", length = 50)
	private String funcao;

	@Column(name = "NU_RAMAL")
	private String ramal;

	@Column(name = "DS_LOTACAO", length = 100)
	private String lotacao;

	@Column(name = "NU_BANCO",  length = 10)
	private String numeroBanco;

	@Column(name = "NN_BANCO",  length = 200)
	private String nomeBanco;

	@Column(name = "NU_AGENCIA",  length = 10)
	private String agencia;

	@Column(name = "NU_CONTA", length = 30)
	private String conta;

	@Column(name = "CD_SITUACAO")
	@Convert(converter = SituacaoUsuario.Converter.class)
	private SituacaoUsuario situacao;

	@Column(name = "IN_GESTOR")
	private boolean gestor;

	@Column(name = "IN_SENHA_EXPIRADA")
	private boolean senhaExpirada;
	
	@Column(name = "IN_USUARIO_SISTEMA")
	private boolean usuarioSistema;
	
	@Transient
	private String getDescricao() {
		return matricula + " - " + nome;
	} 

}
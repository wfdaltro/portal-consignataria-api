package br.com.consigus.portalservidor.entity;

import java.time.LocalDateTime;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.consigus.portalservidor.enums.TipoOperacaoSistema;
import br.com.consigus.portalservidor.security.JwtUsuarioAutenticado;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_TRILHA_AUDITORIA")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_TRILHA_AUDITORIA"))
public class TrilhaAuditoria extends Entidade<Long> {

	@Column(name = "CD_TIPO_OPERACAO", nullable = false)
	@Convert(converter = TipoOperacaoSistema.Converter.class)
	private TipoOperacaoSistema tipoOperacaoSistema;

	@Column(name = "DS_TABELA")
	private String tabela;
	
	@Column(name = "DS_FUNCIONALIDADE")
	private String funcionalidade;
	
	@Column(name = "DS_USUARIO_OPERACAO")
	private String usuarioOperacao;
	
	@Column(name = "DH_INCLUSAO")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime dataHoraInclusao;

	@Column(name = "DS_VALOR_ANTES_ATUALIZACAO", length = 10000)
	private String valorAntesAtualizacao;

	@Column(name = "DS_VALOR_APOS_ATUALIZACAO", length = 10000)
	private String valorAposAtualizacao;
	
	@PrePersist
	public void prePersist() {
		LocalDateTime now = LocalDateTime.now();
		 setDataHoraInclusao(now);
		 setUsuarioOperacao(JwtUsuarioAutenticado.getLoginUsuarioAutenticado());
	}

}

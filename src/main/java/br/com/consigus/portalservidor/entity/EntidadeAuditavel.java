package br.com.consigus.portalservidor.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
@EntityListeners(EntidadeAuditavelListener.class)
public abstract class EntidadeAuditavel<PK> extends EntidadeVersionavel<PK> {

	@Column(name = "DS_LOGIN_USUARIO_INCLUSAO", length = 20)
	private String usuarioInclusao;

	@Column(name = "DH_INCLUSAO")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime dataHoraInclusao;

	@Column(name = "DS_LOGIN_USUARIO_ALTERACAO", length = 20)
	private String usuarioAlteracao;

	@Column(name = "DH_ALTERACAO")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime dataHoraAlteracao;

}

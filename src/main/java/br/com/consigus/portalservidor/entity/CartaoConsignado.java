package br.com.consigus.portalservidor.entity;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.consigus.portalservidor.enums.SituacaoCartao;
import br.com.consigus.portalservidor.enums.SituacaoCartao;
import br.com.consigus.portalservidor.enums.SituacaoConsignacao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_CARTAO_CONSIGNADO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_CARTAO_CONSIGNADO"))
public class CartaoConsignado extends EntidadeAuditavel<Long> {
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SERVIDOR", unique = false, nullable = true, updatable = true)
	private Servidor servidor;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ENTIDADE_CONSIGNATARIA", unique = false, nullable = true, updatable = true)
	private EntidadeConsignataria consignataria;
	
	@Column(name = "CD_SITUACAO_CONSIGNACAO")
	@Convert(converter = SituacaoCartao.Converter.class)
	private SituacaoCartao situacao;
	
	
	@Column(name = "NU_ADE")
	private String ade;
	
	@Column(name = "VL_JUROS")
	private Double jurosMes;

	@Column(name = "VL_LIMITE_CARTAO")
	private BigDecimal valorLimiteCartao;

	@Column(name = "VL_PRECENTUAL_SAQUE")
	private Double percentualSaqueCartao;

	@Column(name = "VL_LIMITE_SAQUE_CARTAO")
	private BigDecimal valorSaqueCartao;

	@Column(name = "VL_JUROS_ANO")
	private Double jurosAno;


}

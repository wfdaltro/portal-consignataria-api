package br.com.consigus.portalservidor.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_PARCELA_EMPRESTIMO_CONSIGNADO")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_PARCELA_EMPRESTIMO_CONSIGNADO"))
public class ParcelasEmprestimoConsignado extends Entidade<Integer> {
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_EMPRESTIMO_CONSIGNADO")
	@JsonIgnore
	private EmprestimoConsignado emprestimoConsignado;
	
	@Column(name = "NU_PARCELA")
	private Integer numero;
	
	@Column(name = "VL_PRINCIPAL")
	private BigDecimal valorPrincipal;
	
	@Column(name = "VL_JUROS")
	private BigDecimal juros;
	
	@Column(name = "VL_TOTAL")
	private BigDecimal valorTotal;
	
	@Column(name = "DT_VNCIMENTO")
	@JsonFormat(pattern = "MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer	.class)
	private LocalDate dataVencimento;
	
	@Column(name = "IN_PAGO")
	private boolean paga;

}

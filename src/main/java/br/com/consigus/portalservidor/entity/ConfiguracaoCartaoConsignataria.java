package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_CONFIGURACAO_CARTAO_CONSIGNATARIA")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_CONFIGURACAO_CARTAO_CONSIGNATARIA"))
public class ConfiguracaoCartaoConsignataria extends EntidadeAuditavel<Long> {

	@Column(name = "VL_TAXA_JUROS")
	private double juros;

	@Column(name = "VL_TAXA_JUROS_ANO")
	private double jurosAnual;

	@Column(name = "VL_PERCENTUAL_SAQUE")
	private double percentualSaqueCartao;

	@Column(name = "NU_MULTIPLICADOR_LIMITE")
	private Integer multipladorLimiteCartao;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRODUTO_CONSIGNATARIA", nullable = true)
	private ProdutoConsignataria produto;

}

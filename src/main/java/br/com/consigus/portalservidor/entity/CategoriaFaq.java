package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_CATEGORIA_FAQ")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_CATEGORIA_FAQ"))
public class CategoriaFaq extends EntidadeAuditavel<Long> {

	@Column(name = "DS_NOME")
	private String nome;

	@Column(name = "IN_ATIVO")
	private boolean ativo;

}

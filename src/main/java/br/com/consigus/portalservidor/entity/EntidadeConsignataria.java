package br.com.consigus.portalservidor.entity;

import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.consigus.portalservidor.enums.Estado;
import br.com.consigus.portalservidor.enums.SituacaoEntidadeConsignataria;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_ENTIDADE_CONSIGNATARIA")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_ENTIDADE_CONSIGNATARIA"))
public class EntidadeConsignataria extends EntidadeAuditavel<Long> {

	@Column(name = "NU_CNPJ", nullable = false, length = 150)
	private String cnpj;

	@Column(name = "DS_NOME", nullable = false, length = 150)
	private String nome;

	@Column(name = "DS_NOME_FANTASIA", nullable = false, length = 150)
	private String nomeFantasia;

	@Column(name = "DT_INICIO_CONTRATO")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dtInicioContrato;

	@Column(name = "DT_FIM_CONTRATO")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dtFimContrato;

	@Column(name = "DS_LOGRADOURO", length = 150)
	private String logradouro;

	@Column(name = "DS_COMPLEMENTO_LOGRADOURO", length = 100)
	private String complemento;

	@Column(name = "DS_BAIRRO", length = 100)
	private String bairro;

	@Column(name = "DS_LOCALIDADE", length = 100)
	private String localidade;

	@Column(name = "NU_CEP", length = 10)
	private String cep;

	@Column(name = "CD_ESTADO")
	@Convert(converter = Estado.Converter.class)
	private Estado estado;

	@Column(name = "DS_NOME_REPRESENTANTE", length = 150)
	private String nomeRepresentante;

	@Column(name = "DS_EMAIL_REPRESENTANTE", length = 150)
	private String emailRepresentante;

	@Column(name = "NU_TELEFONE_REPRESENTANTE", length = 150)
	private String telefoneRepresentante;

	@Column(name = "DS_EMAIL", length = 150)
	private String email;

	@Column(name = "NU_TELEFONE")
	private String telefone;

	@Column(name = "DS_NOME_PRESIDENTE")
	private String presidente;
	
	@Column(name = "DS_SITE", length = 100)
	private String site;

	@Column(name = "DS_EMAIL_OUVIDORIA", length = 100)
	private String emailOuvidoria;

	@Column(name = "DS_SITE_OUVIDORIA", length = 100)
	private String siteOuvidoria;

	@Column(name = "NU_TELEFONE_OUVIDORIA", length = 20)
	private String telefoneOuvidoria;

	@Column(name = "NU_DDG_OUVIDORIA", length = 20)
	private String ddgOuvidoria;

	@Column(name = "CD_SITUACAO")
	@Convert(converter = SituacaoEntidadeConsignataria.Converter.class)
	private SituacaoEntidadeConsignataria situacao;

	@Column(name = "DS_OBSERVACOES")
	private String observacoes;
	
	@Column(name = "NU_BANCO",  length = 10)
	private String numeroBanco;

	@Column(name = "NN_BANCO",  length = 400)
	private String nomeBanco;

	@Column(name = "NU_AGENCIA",  length = 10)
	private String agencia;

	@Column(name = "NU_CONTA", length = 20)
	private String conta;

	@Column(name = "DS_FAVORECIDO")
	private String favorecido;
	
}

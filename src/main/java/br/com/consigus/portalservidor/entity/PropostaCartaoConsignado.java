package br.com.consigus.portalservidor.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import br.com.consigus.portalservidor.enums.SituacaoProposta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_PROPOSTA_CARTAO")
@Data
@EqualsAndHashCode(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@AttributeOverride(name = "pk", column = @Column(name = "ID_PROPOSTA_CARTAO"))
public class PropostaCartaoConsignado extends EntidadeAuditavel<Long> {
	
	@Column(name = "NU_PROPOSTA")
	private String numeroProposta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_OPERADOR_APROVACAO", unique = false, nullable = true, updatable = false)
	private OperadorEntidadeConsignataria operadorAprovacao;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_OPERADOR_RESPONSAVEL", unique = false, nullable = true, updatable = false)
	private OperadorEntidadeConsignataria operadorResponsavel;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CONSIGNATARIA", unique = false, nullable = false, updatable = false)
	private EntidadeConsignataria consignataria;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_SERVIDOR", unique = false, nullable = false, updatable = false)
	private Servidor servidor;

	@Column(name = "VL_JUROS")
	private Double jurosMes;

	@Column(name = "VL_LIMITE_CARTAO")
	private BigDecimal valorLimiteCartao;

	@Column(name = "VL_PRECENTUAL_SAQUE")
	private Double percentualSaqueCartao;

	@Column(name = "VL_LIMITE_SAQUE_CARTAO")
	private BigDecimal valorSaqueCartao;

	@Column(name = "VL_JUROS_ANO")
	private Double jurosAno;

	@Column(name = "DT_VALIDADE_PROPOSTA")
	@JsonFormat(pattern = "dd/MM/yyyy")
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate dataValidadeProposta;
	
	@Column(name = "DS_OBSERVACOES")
	private String observacoes; 
	
	@Column(name = "DS_TOKEN_AVERBACAO")
	private String tokenLiberacao;
	
	@Column(name = "CD_SITUACAO_PROPOSTA")
	@Convert(converter = SituacaoProposta.Converter.class)
	private SituacaoProposta situacao;


}

package br.com.consigus.portalservidor.entity;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.consigus.portalservidor.enums.SituacaoPortabilidade;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_PORTABILIDADE")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_PORTABILIDADE"))
public class Portabilidade extends EntidadeAuditavel<Long> {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_EMPRESTIMO_CONSIGNADO", unique = false, nullable = true, updatable = true)
	private EmprestimoConsignado emprestimo;

	@Column(name = "VL_PRESTACAO")
	private BigDecimal valorPrestacao;

	@Column(name = "NU_PARCELAS")
	private Integer totalDeParcelas;

	@Column(name = "VL_JUROS_MENSAL")
	private Double jurosAoMes;

	@Column(name = "VL_CET")
	private Double cet;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ENTIDADE_CONSIGNATARIA", unique = false, nullable = true, updatable = true)
	private EntidadeConsignataria consignataria;

	
	@Column(name = "CD_SITUACAO_PORTABILIDADE")
	@Convert(converter = SituacaoPortabilidade.Converter.class)
	private SituacaoPortabilidade situacao;
	
	@Column(name = "DS_TOKEN_AVERBACAO")
	private String tokenLiberacao;

}

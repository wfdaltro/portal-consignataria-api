package br.com.consigus.portalservidor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Table(name = "TB_SEQUENCIAL")
@Data
@Entity
public class Sequencial {

	@Id
	private Integer pk;

	@Column(name = "CD_TIPO_SEQUENCIAL")
	private Integer tipoSequencial;

	@Column(name = "NU_ULTIMO_SEQUENCIAL")
	private String numeroUltimoSequencial;

}
package br.com.consigus.portalservidor.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class EntidadeVersionavel<K> extends Entidade<K> {

    @Version
    @Column(name = "NU_VERSAO")
    private Long versao;

}

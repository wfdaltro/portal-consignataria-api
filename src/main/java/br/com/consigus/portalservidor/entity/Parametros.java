package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_PARAMETROS")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_PARAMETROS"))
public class Parametros extends EntidadeAuditavel<Integer> {
	
	@Column(name = "NU_MAXIMO_PARCELAS")
	private Integer numeroMaximoParcelas;
	
	@Column(name = "VL_MAXIMO_PARCELA")
	private Double valorMaximoParcela;
	
	@Column(name = "NU_DIAS_MAXIMO_APROVACAO_CONTRATO")
	private Integer prazoMaximoAprovacaoContrato;
	
	@Column(name = "DS_PROTOCOLO_ENVIO_EMAIL")
	private String protocoloEnvioEmail;
	
	@Column(name = "NU_PORTA_ENVIO_EMAIL")
	private Integer portaEnvioEmail;
	
	@Column(name = "DS_HOST_SERVIDOR_ENVIO_EMAIL")
	private String servidorEnvioEmail;
	
	
	@Column(name = "DS_USUARIO_ENVIO_EMAIL")
	private String usuarioEnvioEmail;
	
	@Column(name = "DS_SENHA_ENVIO_EMAIL")
	private String senhaEnvioEmail;
	
	@Column(name = "DS_REMETENTE_EMAIL_SISTEMA")
	private String remetenteEmailSistema;
	
	
	

}

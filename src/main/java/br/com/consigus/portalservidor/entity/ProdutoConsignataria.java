package br.com.consigus.portalservidor.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.consigus.portalservidor.enums.ProdutoConsignado;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "TB_PRODUTO_CONSIGNATARIA")
@Data
@EqualsAndHashCode(callSuper = true)
@AttributeOverride(name = "pk", column = @Column(name = "ID_PRODUTO_CONSIGNATARIA"))
public class ProdutoConsignataria extends EntidadeAuditavel<Long> {
	
	@Column(name = "CD_PRODUTO_CONSIGNADO")
	@Convert(converter = ProdutoConsignado.Converter.class)
	private ProdutoConsignado produto;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CONSIGNATARIA", unique = false, nullable = false, updatable = true)
	private EntidadeConsignataria entidadeConsignataria;

	@Column(name = "IN_ATIVO")
	public boolean ativo;
	

}

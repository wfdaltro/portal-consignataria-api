package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum TipoRegistroPortabilidade implements EnumModel<String> {

	ENVIADAS("1", "Enviadas"), //
	RECEBIDAS("2", "Recebidas");
	

	private final String descricao;

	private final String codigo;

	TipoRegistroPortabilidade(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<TipoRegistroPortabilidade, String> {
		public Converter() {
			super(TipoRegistroPortabilidade.class);
		}
	}

	public static TipoRegistroPortabilidade get(String codigo) {
		return EnumModelHelper.get().obterInstancia(TipoRegistroPortabilidade.values(), codigo);
	}

}
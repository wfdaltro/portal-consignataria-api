package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum SituacaoConsignacao implements EnumModel<String> {

	ATIVA("1", "Ativo"), //
	SUSPENSA("2", "Suspenso"), //
	LIQUIDADA("3", "Liquidado"), //
	CONSOLIDADA("4", "Consolidado"),
	COMPRADA("5", "Portabilidade");

	private final String descricao;

	private final String codigo;

	SituacaoConsignacao(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<SituacaoConsignacao, String> {
		public Converter() {
			super(SituacaoConsignacao.class);
		}
	}

	public static SituacaoConsignacao get(String codigo) {
		return EnumModelHelper.get().obterInstancia(SituacaoConsignacao.values(), codigo);
	}

}
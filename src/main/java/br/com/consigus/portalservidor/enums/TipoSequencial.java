package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum TipoSequencial implements EnumModel<Integer> {

	PROPOSTA_CARTAO(1, "Proposta Cartão"), PROPOSTA_EMPRESTIMO(2, "Proposta Emprestimo");

	private Integer codigo;

	private String descricao;

	private TipoSequencial(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<TipoSequencial, Integer> {
		public Converter() {
			super(TipoSequencial.class);
		}
	}

	public static TipoSequencial get(Integer codigo) {
		return EnumModelHelper.get().obterInstancia(TipoSequencial.values(), codigo);
	}

}

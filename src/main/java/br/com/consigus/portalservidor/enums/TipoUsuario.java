package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum TipoUsuario implements EnumModel<String> {

	SERVIDOR("S", "Servidor"), GESTOR("G", "Gestor"), CONSIGNATARIA("C", "Consignatária");

	private String codigo;

	private String descricao;

	private TipoUsuario(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<TipoUsuario, String> {
		public Converter() {
			super(TipoUsuario.class);
		}
	}

	public static TipoUsuario get(String codigo) {
		return EnumModelHelper.get().obterInstancia(TipoUsuario.values(), codigo);
	}

}
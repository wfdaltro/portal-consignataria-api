package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum SituacaoEntidadeConsignataria implements EnumModel<String> {

	ATIVO("A", "Ativa"), CANCELADO("I", "Inativa"), BLOQUEADO("B", "Bloqueada"), ENCERRADO("E", "Contrato Encerrado");

	private String codigo;

	private String descricao;

	SituacaoEntidadeConsignataria(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<SituacaoEntidadeConsignataria, String> {
		public Converter() {
			super(SituacaoEntidadeConsignataria.class);
		}
	}

	public static SituacaoEntidadeConsignataria get(String codigo) {
		return EnumModelHelper.get().obterInstancia(SituacaoEntidadeConsignataria.values(), codigo);
	}

}

package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum TipoOperacaoSistema implements EnumModel<String> {

	INCLUSAO("I", "Inclusão"), ALTERACAO("A", "Alteração"), EXCLUSAO("E", "Exclusão");

	private String codigo;

	private String descricao;

	private TipoOperacaoSistema(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<TipoOperacaoSistema, String> {
		public Converter() {
			super(TipoOperacaoSistema.class);
		}
	}

	public static TipoOperacaoSistema get(String codigo) {
		return EnumModelHelper.get().obterInstancia(TipoOperacaoSistema.values(), codigo);
	}

}

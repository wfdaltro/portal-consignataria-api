package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum SituacaoPortabilidade implements EnumModel<String> {

	EM_ANALISE("1", "Em andamento"), //
	ACEITA("2", "Aceita"), //
	CANCELADA("3", "Cancelada"), //
	CONCLUIDA("4", "Concluída");
	

	private final String descricao;

	private final String codigo;

	SituacaoPortabilidade(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<SituacaoPortabilidade, String> {
		public Converter() {
			super(SituacaoPortabilidade.class);
		}
	}

	public static SituacaoPortabilidade get(String codigo) {
		return EnumModelHelper.get().obterInstancia(SituacaoPortabilidade.values(), codigo);
	}

}
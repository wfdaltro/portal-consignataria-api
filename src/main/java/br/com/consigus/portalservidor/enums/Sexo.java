package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum Sexo implements EnumModel<String> {

	MASCULINO("M", "Maculino"), FEMININO("F", "Feminino");

	private final String descricao;

	private final String codigo;

	Sexo(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<Sexo, String> {
		public Converter() {
			super(Sexo.class);
		}
	}

	public static Sexo get(String codigo) {
		return EnumModelHelper.get().obterInstancia(Sexo.values(), codigo);
	}

}

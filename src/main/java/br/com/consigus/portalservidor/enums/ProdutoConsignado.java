package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum ProdutoConsignado implements EnumModel<String> {

	EMPRESTIMO("E", "Empréstimo Consignado"), //
	CARTAO("C", "Cartão de Crédito Consignado"),//
	RENEGOCIACAO_EMPRESTIMO("R", "Renegociação Empréstimo Consignado"); //
//	FINANCIAMENTO_IMOBILIARIO("I", "Financiamento Imobiliário");

	private final String codigo;

	private final String descricao;

	private ProdutoConsignado(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<ProdutoConsignado, String> {
		public Converter() {
			super(ProdutoConsignado.class);
		}
	}

	public static ProdutoConsignado get(String codigo) {
		return EnumModelHelper.get().obterInstancia(ProdutoConsignado.values(), codigo);
	}

}

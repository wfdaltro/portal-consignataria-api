package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum MotivoBloqueioServidor implements EnumModel<String> {

	DESCUMPRIMENTO_CONTRATO("A", "Necessidade Administrativa"), SUSPEITA_FRAUDE("F", "Suspeita de fraude");

	private String codigo;

	private String descricao;

	MotivoBloqueioServidor(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<MotivoBloqueioServidor, String> {
		public Converter() {
			super(MotivoBloqueioServidor.class);
		}
	}

	public static MotivoBloqueioServidor get(String codigo) {
		return EnumModelHelper.get().obterInstancia(MotivoBloqueioServidor.values(), codigo);
	}

}
package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum DestinatarioAviso implements EnumModel<String> {

	TODOS("T", "Todos"), GESTORES("G", "Gestores"), SERVIDORES("S", "Servidores"), MASTERS("M", "Masters"), OPERADORES("O", "Operadores"), USUARIO("U", "usuario");

	private final String codigo;

	private final String descricao;

	private DestinatarioAviso(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

 

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<DestinatarioAviso, String> {
		public Converter() {
			super(DestinatarioAviso.class);
		}
	}

	public static DestinatarioAviso get(String codigo) {
		return EnumModelHelper.get().obterInstancia(DestinatarioAviso.values(), codigo);
	}

}

package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum MotivoBloqueioEntidadeConsignataria implements EnumModel<String> {

	DESCUMPRIMENTO_CONTRATO("D", "Descumprimento contratual "), SUSPEITA_FRAUDE("S", "Suspeita de fraude");

	private String codigo;

	private String descricao;

	MotivoBloqueioEntidadeConsignataria(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<MotivoBloqueioEntidadeConsignataria, String> {
		public Converter() {
			super(MotivoBloqueioEntidadeConsignataria.class);
		}
	}

	public static MotivoBloqueioEntidadeConsignataria get(String codigo) {
		return EnumModelHelper.get().obterInstancia(MotivoBloqueioEntidadeConsignataria.values(), codigo);
	}

}
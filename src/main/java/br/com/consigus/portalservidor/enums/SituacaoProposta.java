package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum SituacaoProposta implements EnumModel<String> {

	EM_ANALISE("1", "Análise"), //
	DEFERIDA("2", "Deferida"), //
	INDEFERIDA("3", "Indeferida"), //
	CANCELADA_PELO_SOLICITANTE("4", "Cancelada"),//
	DEVOLVIDA_AO_SOLICITANTE("5", "Devolvida"),//
	EXPIRADA("6", "Expirada");

	private final String descricao;

	private final String codigo;

	SituacaoProposta(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<SituacaoProposta, String> {
		public Converter() {
			super(SituacaoProposta.class);
		}
	}

	public static SituacaoProposta get(String codigo) {
		return EnumModelHelper.get().obterInstancia(SituacaoProposta.values(), codigo);
	}

}
package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum EstadoCivil implements EnumModel<String> {

	SOLTEIRO("S", "Solteiro(a)"), CASADO("C", "Casado(a)"), DIVORCIADO("D", "Divorciado(a)"), VIUVO("V", "Viúvo(a)");

	private String codigo;

	private String descricao;

	EstadoCivil(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<EstadoCivil, String> {
		public Converter() {
			super(EstadoCivil.class);
		}
	}

	public static EstadoCivil get(String codigo) {
		return EnumModelHelper.get().obterInstancia(EstadoCivil.values(), codigo);
	}

}
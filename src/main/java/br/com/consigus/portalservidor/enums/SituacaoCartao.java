package br.com.consigus.portalservidor.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.consigus.portalservidor.jpaconverters.EnumModelConverter;
import lombok.Getter;

@Getter
@JsonDeserialize(using = EnumModelDeserializer.class)
public enum SituacaoCartao implements EnumModel<String> {

	ATIVA("1", "Ativo"), //
	SUSPENSA("2", "Suspenso"),
	CANCELADO("2", "Cancelado");


	private final String descricao;

	private final String codigo;

	SituacaoCartao(String codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	@javax.persistence.Converter(autoApply = true)
	public static class Converter extends EnumModelConverter<SituacaoCartao, String> {
		public Converter() {
			super(SituacaoCartao.class);
		}
	}

	public static SituacaoCartao get(String codigo) {
		return EnumModelHelper.get().obterInstancia(SituacaoCartao.values(), codigo);
	}

}